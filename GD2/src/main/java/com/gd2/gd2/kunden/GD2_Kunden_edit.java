/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.kunden;

import com.gd2.gd2.init.GD2_Kunden;
import com.vaadin.annotations.Theme;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 *
 * @author temp
 */
    
@Theme("valo")
public class GD2_Kunden_edit {
    
    public VerticalLayout edit_layout = new VerticalLayout();
    public HorizontalLayout first_row = new HorizontalLayout();
    public HorizontalLayout second_row = new HorizontalLayout();
    public HorizontalLayout third_row = new HorizontalLayout();
    public Window edit_window = new Window("Kunden Bearbeiten");
    
    
    public Window getWindow(final Object column){
    
    edit_layout.setMargin(true);
    edit_layout.setSpacing(true);
    
    Object datasource = GD2_Kunden.applicants.getItem(column);

         final TextField firmenname = new TextField(GD2_Kunden.applicants.getItem(column).getItemProperty("firmenname"));
         firmenname.setCaption("Firmenname:");
        
        //firmenname.addValidator(new StringLengthValidator("The name must be 1-10 letters (was {0})", 1, 10, true)); 
        //firmenname.setNullRepresentation(""); 
        //firmenname.setNullSettingAllowed(false);
        //firmenname.setImmediate(true);
        
        final TextField anschrift = new TextField(GD2_Kunden.applicants.getItem(column).getItemProperty("anschrift"));
        anschrift.setCaption("Anschrift:");
        final TextField plz = new TextField(GD2_Kunden.applicants.getItem(column).getItemProperty("PLZ"));
        plz.setCaption("PLZ:");
        final TextField ort = new TextField(GD2_Kunden.applicants.getItem(column).getItemProperty("ort"));
        ort.setCaption("Ort:");
        
        edit_layout.addComponent(firmenname);
        edit_layout.setComponentAlignment(firmenname, Alignment.TOP_CENTER);
        first_row.addComponent(anschrift);
        first_row.addComponent(plz);
        first_row.addComponent(ort);
        
        
        first_row.setMargin(true);
        first_row.setSpacing(true);
        
        edit_layout.addComponent(first_row);
        edit_layout.setComponentAlignment(first_row, Alignment.TOP_CENTER);
        
        final TextField branche = new TextField(GD2_Kunden.applicants.getItem(column).getItemProperty("branche"));
        branche.setCaption("Branche:");
        final TextField ansprechpartner = new TextField(GD2_Kunden.applicants.getItem(column).getItemProperty("ansprechpartner"));
        ansprechpartner.setCaption("Ansprechpartner:");
        final TextField ustid = new TextField("UStID");
        
        second_row.addComponent(branche);
        second_row.addComponent(ansprechpartner);
        second_row.addComponent(ustid);
        
        second_row.setMargin(true);
        second_row.setSpacing(true);
        
        edit_layout.addComponent(second_row);
        edit_layout.setComponentAlignment(second_row, Alignment.TOP_CENTER);
        
        Button save = new Button("Speichern");
        
        edit_layout.addComponent(save);
        edit_layout.setComponentAlignment(save, Alignment.TOP_CENTER);
        
        save.addClickListener(new Button.ClickListener() {

            
            public void buttonClick(Button.ClickEvent event) {
        
          GD2_Kunden.applicants.removeItem(column);
          GD2_Kunden.applicants.addBean(new GD2_Kunden_datasource(firmenname.getValue(),anschrift.getValue(), plz.getValue(), ort.getValue(), branche.getValue(), ansprechpartner.getValue()));
        edit_window.close();
         }});
        
    
    
    
    
    
  
    
        edit_window.setWidth(70, Sizeable.Unit.PERCENTAGE);
        edit_window.setHeight(80, Sizeable.Unit.PERCENTAGE);
        edit_window.center();
    
        UI ui = UI.getCurrent();
        edit_window.setContent(edit_layout);
        ui.addWindow(edit_window);
    
    
    return edit_window;
    }
}
