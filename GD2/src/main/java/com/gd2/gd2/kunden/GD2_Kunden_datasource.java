/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.kunden;

import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.vaadin.data.Container;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author temp
 */
public class GD2_Kunden_datasource implements Serializable{
    
    String firmenname;
    String anschrift;
    String plz;
    String ort;
    String branche;
    String ansprechpartner;
    String[] projekte;

public GD2_Kunden_datasource(String firmenname, String anschrift, String plz, String ort, String branche, String ansprechpartner ) {
this.firmenname = firmenname;
this.anschrift = anschrift;
this.plz = plz;
this.ort = ort;
this.branche = branche;
this.ansprechpartner = ansprechpartner;
//this.projekte = projekte;
}
  
    
    
public String getFirmenname () {

return firmenname;
}


public void setFirmenname (String firmenname) {

this.firmenname = firmenname;
}

public String getAnschrift () {

return anschrift;
}


public void setAnschrift (String anschrift) {

this.anschrift = anschrift;
}

public String getPLZ () {

return plz;
}


public void setPLZ (String plz) {

this.plz = plz;
}


public String getOrt () {

return ort;
}


public void setOrt (String ort) {

this.ort = ort;
}

public String getBranche () {

return branche;
}


public void setBranche (String branche) {

this.branche = branche;
}

public String getAnsprechpartner () {

return ansprechpartner;
}


public void setAnsprechpartner (String ansprechpartner) {

this.ansprechpartner = ansprechpartner;
}

public String[] getProjekte () {

return projekte;
}


public void setProjekte (String[] projekte) {

this.projekte = projekte;
}

}

