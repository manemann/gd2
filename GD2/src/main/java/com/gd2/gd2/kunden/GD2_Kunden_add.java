/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.kunden;

import com.gd2.gd2.init.GD2_Kunden;
import com.vaadin.annotations.Theme;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import org.vaadin.tokenfield.*;

/**
 *
 * @author temp
 */

@Theme("valo")
public class GD2_Kunden_add {
    
    public VerticalLayout add_layout = new VerticalLayout();
    public HorizontalLayout first_row = new HorizontalLayout();
    public HorizontalLayout second_row = new HorizontalLayout();
    public HorizontalLayout third_row = new HorizontalLayout();
    public Window add_window = new Window("Kunden Hinzufügen");
    public TokenField f = new TokenField("Tags");

    
    public Window getWindow () {
        
        add_layout.setMargin(true);
        add_layout.setSpacing(true);
        
        
        final TextField firmenname = new TextField("Firmenname");
        
        firmenname.addValidator(new StringLengthValidator("The name must be 1-10 letters (was {0})", 1, 10, true)); 
        firmenname.setNullRepresentation(""); 
        firmenname.setNullSettingAllowed(false);
        firmenname.setImmediate(true);
        
        final TextField anschrift = new TextField("Anschrift");
        final TextField plz = new TextField("PLZ");
        final TextField ort = new TextField("Ort");
        
        add_layout.addComponent(firmenname);
        add_layout.setComponentAlignment(firmenname, Alignment.TOP_CENTER);
        first_row.addComponent(anschrift);
        first_row.addComponent(plz);
        first_row.addComponent(ort);
        
        
        first_row.setMargin(true);
        first_row.setSpacing(true);
        
        add_layout.addComponent(first_row);
        add_layout.setComponentAlignment(first_row, Alignment.TOP_CENTER);
        
        final TextField branche = new TextField("Branche");
        final TextField ansprechpartner = new TextField("Ansprechpartner");
        final TextField ustid = new TextField("UStID");
        
        second_row.addComponent(branche);
        second_row.addComponent(ansprechpartner);
        second_row.addComponent(ustid);
        
        second_row.setMargin(true);
        second_row.setSpacing(true);
        
        add_layout.addComponent(second_row);
        add_layout.setComponentAlignment(second_row, Alignment.TOP_CENTER);
        
        Button save = new Button("Speichern");

                
        add_layout.addComponent(save);
        add_layout.setComponentAlignment(save, Alignment.TOP_CENTER);
           
        f.removeAllValidators();
        f.setFilteringMode(FilteringMode.OFF);
        add_layout.addComponent(f);
        
        save.addClickListener(new Button.ClickListener() {

            
            public void buttonClick(Button.ClickEvent event) {
        
        GD2_Kunden.applicants.addBean(new GD2_Kunden_datasource(firmenname.getValue(),anschrift.getValue(), plz.getValue(), ort.getValue(), branche.getValue(), ansprechpartner.getValue()));
        add_window.close();
         }});
        
        
        
        
        add_window.setWidth(70, Sizeable.Unit.PERCENTAGE);
        add_window.setHeight(80, Sizeable.Unit.PERCENTAGE);
        add_window.center();
    
        UI ui = UI.getCurrent();
        add_window.setContent(add_layout);
        ui.addWindow(add_window);
    
    
    return add_window;
    }
    
}
