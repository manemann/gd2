package com.gd2.gd2.kunden;

import com.gd2.gd2.init.GD2_Kunden;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author temp
 */
public class GD2_Kunden_details {
    
    
    
    public VerticalLayout details_layout = new VerticalLayout();
    public HorizontalLayout first_row = new HorizontalLayout();
    public HorizontalLayout second_row = new HorizontalLayout();
    public HorizontalLayout third_row = new HorizontalLayout();
    public Window details_window = new Window("Kunden Details");
    
    
    
    public Window getWindow(Object column){
    
    details_layout.setMargin(true);
    details_layout.setSpacing(true);
        
        
        final Label firmenname = new Label(GD2_Kunden.applicants.getItem(column).getItemProperty("firmenname"));
        
      
        
        final Label anschrift = new Label(GD2_Kunden.applicants.getItem(column).getItemProperty("anschrift"));
        final Label plz = new Label(GD2_Kunden.applicants.getItem(column).getItemProperty("PLZ"));
        final Label ort = new Label(GD2_Kunden.applicants.getItem(column).getItemProperty("ort"));
        
        details_layout.addComponent(firmenname);
        details_layout.setComponentAlignment(firmenname, Alignment.TOP_CENTER);
        first_row.addComponent(anschrift);
        first_row.addComponent(plz);
        first_row.addComponent(ort);
        
        
        first_row.setMargin(true);
        first_row.setSpacing(true);
        
        details_layout.addComponent(first_row);
        details_layout.setComponentAlignment(first_row, Alignment.TOP_CENTER);
        
        final Label branche = new Label(GD2_Kunden.applicants.getItem(column).getItemProperty("branche"));
        final Label ansprechpartner = new Label(GD2_Kunden.applicants.getItem(column).getItemProperty("ansprechpartner"));
        final Label ustid = new Label("UStID");
        
        second_row.addComponent(branche);
        second_row.addComponent(ansprechpartner);
        second_row.addComponent(ustid);
        
        second_row.setMargin(true);
        second_row.setSpacing(true);
        
        details_layout.addComponent(second_row);
        details_layout.setComponentAlignment(second_row, Alignment.TOP_CENTER);
        

        
    
        details_window.setWidth(70, Sizeable.Unit.PERCENTAGE);
        details_window.setHeight(80, Sizeable.Unit.PERCENTAGE);
        details_window.center();
    
        UI ui = UI.getCurrent();
        details_window.setContent(details_layout);
        ui.addWindow(details_window);
        
        
        
        
   return details_window; 
   
    }
   
}
