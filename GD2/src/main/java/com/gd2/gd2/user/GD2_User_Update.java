/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.user;

import static com.gd2.gd2.user.GD2_User_Add.PassWord;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Page;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;

/**
 *
 * @author temp
 */
public class GD2_User_Update {
    public Window UpdateUser = new Window("Nutzer Bearbeiten");
    
    public VerticalLayout LabelLayout = new VerticalLayout();
    public HorizontalLayout ButtonLayout = new HorizontalLayout();
 
    public Button Save = new Button("Ok");
    public Button Close = new Button("abbrechen");
    public Integer PassWordHash = 0;
    
    
    
    
    
    
    
    
    
    
    
    public Window UpdateUser (final String OldName,final  String UserName,final  String PassWord,final  String FirstName,final  String LastName,final
                                String Phone,final  String Email,final  Integer Site, Integer Pnummer) throws SQLException, NamingException {
        
        
        Save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        Close.addStyleName(ValoTheme.BUTTON_PRIMARY);
        
      Label headline = new Label("Möchten Sie den Nutzer: " + UserName +" tatsächlich Bearbeiten?");
      LabelLayout.addComponent(headline);
        Save.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                    
//                    
//                    try{
//                        
//                        
//            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
//            messageDigest.update(PassWord.getBytes());
//            PassWordHash = new String(messageDigest.digest());
//
//                    }
//                    catch (NoSuchAlgorithmException ex) {
//                         Logger.getLogger(GD2_User_Add.class.getName()).log(Level.SEVERE, null, ex);
//                     }
//         
                    PassWordHash = PassWord.hashCode();
                    
                    
                    
                 GD2_Users_Datasource updateuser = new GD2_Users_Datasource();
                    try {
                        updateuser.UpdateUserDB(OldName, UserName, PassWordHash, FirstName, LastName, Phone, Email, Site, Pnummer);
                    } catch (SQLException ex) {
                        Logger.getLogger(GD2_User_Update.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NamingException ex) {
                        Logger.getLogger(GD2_User_Update.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                  UpdateUser.close();  
             
                Notification.show("Benutzer Bearbeitet",Notification.TYPE_HUMANIZED_MESSAGE);
                    
                
                
                
             
                
                }});
                    
             
        
        
               Close.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                
               UpdateUser.close();
               
                
                }});
        
        
        
        
        
        ButtonLayout.addComponent(Save);
        ButtonLayout.addComponent(Close);
        ButtonLayout.setMargin(true);
        ButtonLayout.setSpacing(true);
        LabelLayout.addComponent(ButtonLayout);
        LabelLayout.setSpacing(true);
        LabelLayout.setMargin(true);
        
        
    
        UpdateUser.setContent(LabelLayout);
        UpdateUser.setHeight(30,Sizeable.Unit.PERCENTAGE);
        UpdateUser.setWidth(30, Sizeable.Unit.PERCENTAGE);
       
        
    return UpdateUser;
    }
    
}
