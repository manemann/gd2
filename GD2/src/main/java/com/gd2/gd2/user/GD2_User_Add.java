/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.user;

import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Page;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;

/**
 *
 * @author temp
 */
public class GD2_User_Add {
    public Window AddUser = new Window("Nutzer Hinzufügen");
    
    public VerticalLayout AddUserLayout = new VerticalLayout();
    public HorizontalLayout ButtonLayout = new HorizontalLayout();
    public VerticalLayout AddUserTab1 = new VerticalLayout();
    public VerticalLayout AddUserTab2 = new VerticalLayout();
    public VerticalLayout AddUserTab3 = new VerticalLayout();
    
    
    public Accordion AddUserAccordion = new Accordion();
    
    public static TextField UserName = new TextField("Username");
    public static PasswordField PassWord = new PasswordField("Password");
    public TextField FirstName = new TextField("Vorname");
    public TextField LastName = new TextField("Nachname");
    public TextField Email = new TextField("Email");
    public TextField Phone = new TextField("Telefon");
    public TextField PNummer = new TextField("Personal Nummer");
    public ComboBox Site = new ComboBox("Niederlassung");
    public ComboBox Group = new ComboBox("Nutzer Gruppe");
    
    public Integer PassWordHash = 0;
    
    
    public Button Save = new Button("Speichern");
    public Button Close = new Button("Schliessen");
    
    
    
    
    
    
    
    
    
    
    
    public Window AddUser () throws SQLException, NamingException {
        
        Save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        Close.addStyleName(ValoTheme.BUTTON_PRIMARY);
        
        AddUserTab1.setCaption("Berechtigungen");
        AddUserTab1.addComponent(Group);
        
        AddUserTab1.setMargin(true);
        AddUserTab1.setSpacing(true);
        
        FormLayout details3 = new FormLayout();
        details3.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
        details3.setMargin(true);
        details3.setSpacing(true);
        

        AddUserLayout.addStyleName("profile-form");
        
        AddUserLayout.addComponent(details3);
        AddUserLayout.setExpandRatio(details3, 1);
        AddUserLayout.setComponentAlignment(details3, Alignment.TOP_RIGHT);
        AddUserLayout.setMargin(true);
        AddUserLayout.setSpacing(true);
   
        
        
        GD2_Users_Datasource groups = new GD2_Users_Datasource();
        groups.Exec_query_groups();
        
        for (int i = 0; i < groups.groups.size(); i++) {
		Group.addItem(groups.groups.get(i));}
        
        
        GD2_Users_Datasource sites = new GD2_Users_Datasource();
        sites.Exec_query_sites();
        
        for (int i = 0; i < sites.sites.size(); i++) {
		Site.addItem(sites.sites.get(i));}
        
//        AddUserTab2.setCaption("Anmeldedaten");
        
        details3.addComponent(UserName);
        
//        AddUserTab2.setMargin(true);
//        AddUserTab2.setSpacing(true);
        
        UserName.addValidator(new StringLengthValidator("der Nutzername muss mind. 5 Zeichen lang sein", 5, 50, true));
        UserName.setValidationVisible(false);
        
        details3.addComponent(PassWord);
        PassWord.addValidator(new StringLengthValidator("Das Password muss mind. 6 Zeichen lang sein", 6, 16, true));
        PassWord.setValidationVisible(false);
        
        // AddUserTab3.setCaption("Nutzerdaten");
        
        details3.addComponent(FirstName);
        details3.addComponent(LastName);
        details3.addComponent(PNummer);
        details3.addComponent(Email);
        details3.addComponent(Phone);
        details3.addComponent(Site);
        details3.addComponent(Group);
        
//        AddUserTab3.setMargin(true);
//        AddUserTab3.setSpacing(true);
        
        
//        
//        Site.addItem("Leverkusen - Hauptsitz");
//        Site.addItem("Troisdorf");
//        Site.addItem("Düsseldorf");
//        Site.addItem("Mülheim a.d. Ruhr");
//        Site.addItem("Monheim");
//        Site.addItem("Neuss");
//        Site.addItem("Solingen");
        
        
        
//        AddUserAccordion.addTab(AddUserTab1);
//        AddUserAccordion.addTab(AddUserTab2);
//        AddUserAccordion.addTab(AddUserTab3);
        
//        AddUserLayout.addComponent(AddUserAccordion);
        
        ButtonLayout.addComponent(Save);
        

        
        UserName.setImmediate(true);
        
        Save.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                    
                 GD2_Users_Validation validator = new GD2_Users_Validation();
                    try {
                        validator.ValidateNewUser(UserName.getValue(), PassWord.getValue());
                    } catch (SQLException ex) {
                        Logger.getLogger(GD2_User_Add.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NamingException ex) {
                        Logger.getLogger(GD2_User_Add.class.getName()).log(Level.SEVERE, null, ex);
                    }
         
                if (validator.validated == true){
                
                        PassWordHash = PassWord.getValue().hashCode();
                    
                    GD2_Users_Datasource addUser = new GD2_Users_Datasource();
                     try {
                         addUser.Exec_query_group_groupid(Group.getItemCaption(Group.getValue()));
                         System.out.println("Gruppe geholt");
                         addUser.Exec_query_user_userid_latest();
                         System.out.println("ID  geholt");
                         addUser.InsertUserDB(addUser.gid,UserName.getValue(), PassWordHash, FirstName.getValue(), LastName.getValue(),
                         Phone.getValue(), Email.getValue(), Site.getTabIndex(), addUser.uid_latest + 1,Integer.valueOf(PNummer.getValue()));
                         System.out.println("User Eingefügt");
                         
                     } catch (SQLException ex) {
                         Logger.getLogger(GD2_User_Add.class.getName()).log(Level.SEVERE, null, ex);
                     } catch (NamingException ex) {
                         Logger.getLogger(GD2_User_Add.class.getName()).log(Level.SEVERE, null, ex);
                     }
                     
                     
                    
              GD2_User getrree = new GD2_User();
              getrree.get_user_tree();
              Page.getCurrent().reload();

 
              

               AddUser.close();
               UserName.setValue("");
               PassWord.setValue("");
               FirstName.setValue("");
               LastName.setValue("");
               Phone.setValue("");
               Site.setValue("");     
                    
                Notification.show("Benutzer Hinzugefügt",Notification.TYPE_HUMANIZED_MESSAGE);
                    
                
                }
                    
                
                }});
        
        
               Close.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                
               AddUser.close();
               UserName.setValue("");
               PassWord.setValue("");
               FirstName.setValue("");
               LastName.setValue("");
               Phone.setValue("");
               Site.setValue("");
                
                }});
        
        
        
        
        
        
        ButtonLayout.addComponent(Close);
        ButtonLayout.setMargin(true);
        ButtonLayout.setSpacing(true);
       
        
        AddUserLayout.addComponent(ButtonLayout);
        
        
        AddUserLayout.setMargin(true);
        AddUserLayout.setSpacing(true);
    
        AddUser.setContent(AddUserLayout);
        AddUser.setHeight(70, Sizeable.Unit.PERCENTAGE);
        AddUser.setWidth(40, Sizeable.Unit.PERCENTAGE);
       
        
    return AddUser;
    }
    
}
