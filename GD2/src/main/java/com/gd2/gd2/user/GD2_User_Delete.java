/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.user;

import com.vaadin.server.Page;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;

/**
 *
 * @author temp
 */
public class GD2_User_Delete {
    public Window DeleteUser = new Window("Nutzer Löschen");
    
    public VerticalLayout LabelLayout = new VerticalLayout();
    public HorizontalLayout ButtonLayout = new HorizontalLayout();
 
    public Button Save = new Button("Löschen");
    public Button Close = new Button("abbrechen");
   
    
    
    
    
    
    
    
    
    
    
    
    public Window DeleteUser (final String UserName) throws SQLException, NamingException {
        
        
        Save.addStyleName(ValoTheme.BUTTON_DANGER);
        Close.addStyleName(ValoTheme.BUTTON_PRIMARY);
        
      Label headline = new Label("Möchten Sie den Nutzer: " + UserName +" tatsächlich Löschen?");
      LabelLayout.addComponent(headline);
        Save.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                    
//                    
//                    try{
//                        
//                        
//            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
//            messageDigest.update(PassWord.getBytes());
//            PassWordHash = new String(messageDigest.digest());
//
//                    }
//                    catch (NoSuchAlgorithmException ex) {
//                         Logger.getLogger(GD2_User_Add.class.getName()).log(Level.SEVERE, null, ex);
//                     }
//         
                    
                    
                    
                    
                 GD2_Users_Datasource updateuser = new GD2_Users_Datasource();
                    try {
                        updateuser.RemoveUserDB(UserName);
                    } catch (SQLException ex) {
                        Logger.getLogger(GD2_User_Delete.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NamingException ex) {
                        Logger.getLogger(GD2_User_Delete.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                  DeleteUser.close();  
             
                Notification.show("Benutzer gelöscht",Notification.TYPE_HUMANIZED_MESSAGE);
                    Page.getCurrent().reload();
                
                
                
             
                
                }});
                    
             
        
        
               Close.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                
               DeleteUser.close();
               
                
                }});
        
        
        
        
        
        ButtonLayout.addComponent(Save);
        ButtonLayout.addComponent(Close);
        ButtonLayout.setMargin(true);
        ButtonLayout.setSpacing(true);
        LabelLayout.addComponent(ButtonLayout);
        LabelLayout.setSpacing(true);
        LabelLayout.setMargin(true);
        
        
    
        DeleteUser.setContent(LabelLayout);
        DeleteUser.setHeight(30,Sizeable.Unit.PERCENTAGE);
        DeleteUser.setWidth(30, Sizeable.Unit.PERCENTAGE);
       
        
    return DeleteUser;
    }
    
}
