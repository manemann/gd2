/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.user;

import com.vaadin.server.Page;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;

/**
 *
 * @author temp
 */
public class GD2_Group_Delete {
    
    
    
      public Window DeleteGroup = new Window("Gruppe Löschen");
    
    public VerticalLayout LabelLayout = new VerticalLayout();
    public HorizontalLayout ButtonLayout = new HorizontalLayout();
 
    public Button Save = new Button("Löschen");
    public Button Close = new Button("abbrechen");
    
    
    
    
    
    
    
    
    
    
    
    
    public Window DeleteGroup (final String GroupName) throws SQLException, NamingException {
        
        Save.addStyleName(ValoTheme.BUTTON_DANGER);
        Close.addStyleName(ValoTheme.BUTTON_PRIMARY);
        Label headline = new Label("Möchten Sie die Gruppe: "+ GroupName +" tatsächlich Löschen?");
        LabelLayout.addComponent(headline);
        
        Save.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                    
                    
                    
                 GD2_Users_Datasource updategroup = new GD2_Users_Datasource();
                    try {
                        updategroup.RemoveGroupDB(GroupName);
                    
                    } catch (SQLException ex) {
                        Logger.getLogger(GD2_Group_Delete.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NamingException ex) {
                        Logger.getLogger(GD2_Group_Delete.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    
                    
                    
            
                    
                    
                   DeleteGroup.close(); 
                    
             
                Notification.show("Gruppe gelöscht",Notification.TYPE_HUMANIZED_MESSAGE);
                    Page.getCurrent().reload();
                
                
                
             
                
                }});
                    
             
        
        
               Close.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                
               DeleteGroup.close();
               
                
                }});
        
        
        
        
        
        ButtonLayout.addComponent(Save);
        ButtonLayout.addComponent(Close);
        ButtonLayout.setMargin(true);
        ButtonLayout.setSpacing(true);
        LabelLayout.addComponent(ButtonLayout);
        LabelLayout.setSpacing(true);
        LabelLayout.setMargin(true);
        
    
        DeleteGroup.setContent(LabelLayout);
        DeleteGroup.setHeight(30, Sizeable.Unit.PERCENTAGE);
        DeleteGroup.setWidth(30, Sizeable.Unit.PERCENTAGE);
       
        
    return DeleteGroup;
    }
    
    
    
    
}
