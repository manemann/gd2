/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.user;

import com.vaadin.data.Validator;
import com.vaadin.ui.Notification;
import java.sql.SQLException;
import javax.naming.NamingException;

/**
 *
 * @author temp
 */
public class GD2_Users_Validation {
    
    
    public Boolean validated = false;
    public Boolean validated_username = false;
    public Boolean validated_password = false;
    
    public Boolean ValidateNewUser (String UserName, String PassWord) throws SQLException, NamingException {
        
        
        final GD2_User_TreeDatasource usernames = new GD2_User_TreeDatasource();
        usernames.getTreeUsers();
        
        
             //Validate Username
         
         try {
         usernames.users.removeAllContainerFilters();
         usernames.users.addContainerFilter("Username", GD2_User_Add.UserName.getValue(), false, true);
         GD2_User_Add.UserName.validate();
         if (!usernames.users.getItemIds().isEmpty() == true) {
             Notification.show("Nutzername bereits vergeben",Notification.TYPE_WARNING_MESSAGE);
             
             
         } else  {

             validated_username = true;
             
             }

         }
         catch (Validator.InvalidValueException e) {
             System.out.println("Die Exec." + e.getMessage());
         Notification.show(e.getMessage(),Notification.TYPE_WARNING_MESSAGE);
         GD2_User_Add.UserName.setValidationVisible(true);
                 
         
        
         
         }    
    
    
    //Validate Password           
                
         
         try {
         GD2_User_Add.PassWord.validate();
         validated_password = true;
         }
         catch (Validator.InvalidValueException e) {

         Notification.show(e.getMessage(),Notification.TYPE_WARNING_MESSAGE);
         GD2_User_Add.PassWord.setValidationVisible(true);
        
         
         }

         
         
         if (validated_username == true && validated_password == true){
         
         validated = true;
         }
           
    System.out.println(validated_username + "-----" + validated_password);
    return validated;
    }}
    
    
    
    

