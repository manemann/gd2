/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.user;



import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;




/**
 *
 * @author temp
 */
public class GD2_Users_Datasource {
  
    
    public Statement st = null;
    public transient Connection con = null;
    public ResultSet rs = null;
    public String username = "";
    public String password = "";
    public String firstname = "";
    public String lastname = "";
    public String email = "";
    public String phone= "";
    public Integer site = 0;
    public Integer userid = 0;
    public Integer groupid = 0;
    public Integer credid = 0;
    public Integer gid = 0;
    public Integer gid_latest = 0;
    public Integer PNummer = 0 ;
    
    public Integer uid = 0;
    public Integer uid_latest = 0;
    
    public String niederlassung_name = "";
    public String niederlassung_adresse = "";
    public String niederlassung_nummer = "";
    public String niederlassung_zusatz = "";
    public Integer niederlassung_PLZ = 0;
    public String niederlassung_phone = "";
    
    
    public String group_name = "";
    public Boolean is_admin = false;
    public Integer admin_level = 0;
    public Boolean is_users = false;
    public Integer user_level = 0;
    public Boolean is_auswertung = false;
    public Integer auswertung_level = 0;
    public Boolean is_calendar = false;
    public Integer calendar_level = 0;
    public Boolean is_projekte = false;
    public Integer projekte_level = 0;
    public Boolean is_kunden = false;
    public Integer kunden_level = 0;
    public Boolean is_mitarbeiter = false;
    public Integer mitarbeiter_level = 0;
    public Boolean is_bewerber = false;
    public Integer bewerber_level = 0;
    public Boolean is_dashboard = false;
    public Integer dashboard_level = 0;
    

    public List groups = new ArrayList();
    public List sites = new ArrayList();
    public List jobs = new ArrayList();
    public List doings = new ArrayList();
    
    
    public Connection Connect_DB() throws SQLException, NamingException {
        
    Context initContext = new InitialContext();
    Context envContext  = (Context)initContext.lookup("java:/comp/env");
    DataSource ds = (DataSource)envContext.lookup("jdbc/GD2_MSSQL");
    con = ds.getConnection();
    return con;
}
    
    
     
    
    public void Exec_query_users (String uname) throws SQLException, NamingException {
    
       
        Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT * FROM dbo.GD2Benutzer WHERE BenutzerName = '" + uname + "'");
         
      
         
         while (rs.next()) {
         
             username = rs.getString("BenutzerName") ;
             password = rs.getString("BenutzerPasswort");
             firstname = rs.getString("BenutzerVorName");
             lastname = rs.getString("BenutzerNachName");
             email = rs.getString("BenutzerEmail");
             phone= rs.getString("BenutzerPhone");
             site = rs.getInt("NiederlassungsID");
             userid = rs.getInt("BenutzerID");
             groupid = rs.getInt("GruppenID");
             PNummer = rs.getInt("BenutzerPNummer");
         
         }
         con.close();
        
         
        
         
    }
    
    public void Exec_query_group () throws SQLException, NamingException {
    
     Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT * FROM dbo.GD2Gruppen WHERE GruppenID = '" + groupid + "'");
         
         while (rs.next()) {
         
                group_name = rs.getString("GruppenName") ;
                is_admin = rs.getBoolean("ZugriffAdministration") ;
                admin_level = rs.getInt("ZugriffAdministrationLevel");

                is_bewerber = rs.getBoolean("ZugriffBewerber") ;
                bewerber_level = rs.getInt("ZugriffBewerberLevel");
                
                is_dashboard = rs.getBoolean("ZugriffBewerber") ;
                dashboard_level = rs.getInt("ZugriffDashboardLevel");
             
             
        
         }
         
         con.close();
         
    }
    
    public void Exec_query_site () throws SQLException, NamingException {
    
     Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT * FROM dbo.GD2Niederlassungen WHERE NiederlassungsID = '" + site + "'");
         
         while (rs.next()) {
         
                niederlassung_name = rs.getString("NiederlassungName") ;
                niederlassung_adresse = rs.getString("NiederlassungAdresseStrasse");
                niederlassung_nummer = rs.getString("NiederlassungAdresseNummer");
                niederlassung_zusatz = rs.getString("NiederlassungAdresseZusatz");
                niederlassung_PLZ = rs.getInt("NiederlassungPLZ");
                niederlassung_phone = rs.getString("NiederlassungTelefon");
       
             
             
        
         }
         
         con.close();
         
    }
         
//          public void Exec_query_credentials () throws SQLException, NamingException {
//    
//        Connect_DB();
//     
//        st = con.createStatement();
//        
//         rs = st.executeQuery("SELECT * FROM gd2_int_db_credentials WHERE USERID = '" + userid + "' AND GROUPID = '" + groupid + "'");
//         
//         while (rs.next()) {
//         
//
//                credid = rs.getInt("CREDENTIALSID") ;
//                is_addmitarbeiter = rs.getBoolean("IS_ADD_MITARBEITER") ;
//                is_modifymitarbeiter = rs.getBoolean("IS_MODIFY_MITARBEITER") ;
//                is_addkunden = rs.getBoolean("IS_ADD_CUSTOMER") ;
//                is_modifykunden = rs.getBoolean("IS_MODIFY_CUSTOMER") ;
//  
//             
//             
//        
//         }
//         
//         con.close();
//    }
//          
//           
          
          public void Exec_query_group_groupname (String groupname) throws SQLException, NamingException {
    
     Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT * FROM dbo.GD2Gruppen WHERE GruppenName = '" + groupname + "'");
         
         while (rs.next()) {
         
                group_name = rs.getString("GruppenName") ;
               is_admin = rs.getBoolean("ZugriffAdministration") ;
                admin_level = rs.getInt("ZugriffAdministrationLevel");

                is_bewerber = rs.getBoolean("ZugriffBewerber") ;
                bewerber_level = rs.getInt("ZugriffBewerberLevel");
                
                is_dashboard = rs.getBoolean("ZugriffBewerber") ;
                dashboard_level = rs.getInt("ZugriffDashboardLevel");
             
             
        
         }
         
         con.close();
         
    }
          
          public List Exec_query_groups () throws SQLException, NamingException {
    
     Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT * FROM dbo.GD2Gruppen");
         
         while (rs.next()) {
         
                groups.add(rs.getString("GruppenName")) ;
               
             
            
        
         }
         
           con.close();
         return groups;
    }
          
          
          
              public List Exec_query_jobs () throws SQLException, NamingException {
    
     Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT * FROM dbo.GD2Berufe");
         
         while (rs.next()) {
         
                jobs.add(rs.getString("BerufBezeichnung")) ;
               
             
            
        
         }
         
           con.close();
         return jobs;
    }     
         
    public List Exec_query_doings () throws SQLException, NamingException {
    
     Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT * FROM dbo.GD2Taetigkeiten");
         
         while (rs.next()) {
         
                doings.add(rs.getString("TaetigkeitBezeichnung")) ;
               
             
            
        
         }
         
           con.close();
          
         return doings;
         
    }            
              
              
     public List Exec_query_sites () throws SQLException, NamingException {
    
     Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT * FROM dbo.GD2Niederlassungen");
         
         while (rs.next()) {
         
                sites.add(rs.getString("NiederlassungName")) ;
               
             
            
        
         }
         
         
         
         con.close();
         System.out.println(sites);
         return sites;
    }
          
           public Integer Exec_query_group_groupid (String groupname) throws SQLException, NamingException {
    
     Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT * FROM dbo.GD2Gruppen WHERE GruppenName = '" + groupname + "'");
         
         while (rs.next()) {
         
           gid = rs.getInt("GruppenID") ;

             
             
        
         }
        
         
         con.close();
         return gid;
    }
           
        public Integer Exec_query_group_groupid_latest () throws SQLException, NamingException {
    
     Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT MAX(GruppenID)AS GruppenID  FROM dbo.GD2Gruppen");
         
         while (rs.next()) {
         
           gid_latest = rs.getInt("GruppenID") ;

             
             
        
         }
        
         
         con.close();
         return gid_latest;
    } 
        
            public Integer Exec_query_user_userid_latest () throws SQLException, NamingException {
    
     Connect_DB();
     
        st = con.createStatement();
        
         rs = st.executeQuery("SELECT MAX(BenutzerID)AS BenutzerID FROM dbo.GD2Benutzer");
         
         while (rs.next()) {
         
           uid_latest = rs.getInt("BenutzerID") ;

             
             
        
         }
        
         
         con.close();
         return uid_latest;
    } 
           
           
           
           
           
          
          
          
          
      public void InsertUserDB (Integer groupid, String UserName, Integer PassWord, String FirstName, String LastName, String Phone, String Email, Integer SiteID, Integer UserID, Integer PNummer) throws SQLException, NamingException {
    
        Connect_DB();
     
        st = con.createStatement();
        
         st.executeUpdate("INSERT INTO dbo.GD2Benutzer(GruppenID,BenutzerName,BenutzerVorName,BenutzerNachName,BenutzerEmail,BenutzerPhone,NiederlassungsID,BenutzerID,BenutzerPasswort, BenutzerPNummer)" +
                               "VALUES('"+groupid+"','" + UserName +"','" + FirstName +"','" + LastName +"','" + Email +"'"+ ",'" + Phone +"','" + SiteID +"','" + UserID +"','" + PassWord +"','" + PNummer +"')");
         
        
         
         con.close();
    }  
      
           public void UpdateUserDB (String OldName, String UserName, Integer PassWord, String FirstName, String LastName, String Phone, String Email, Integer Site, Integer PNummer) throws SQLException, NamingException {
    
        Connect_DB();
     
        st = con.createStatement();
       
         st.executeUpdate("UPDATE dbo.GD2Benutzer SET BenutzerName = '"+ UserName+"',BenutzerVorName = '" + FirstName +"',BenutzerNachName = '" + LastName +"'"
                 + ",BenutzerEmail = '" + Email +"',BenutzerPhone = '" + Phone +"',NiederlassungsID = '" + Site +"',BenutzerPasswort = '" + PassWord +"',BenutzerPNummer = '" + PNummer +"' WHERE BenutzerName = '" + OldName +"'"); 
                               
         
        
         
         con.close();
    }  
           
                public void UpdateGroupDB (String GroupName, Boolean IsAdmin, Integer AdminLevel, Boolean IsDashboard, Integer DashboardLevel,
                                             Boolean IsBewerber, Integer BewerberLevel) throws SQLException, NamingException {
    
        Connect_DB();
     
        st = con.createStatement();
        
         st.executeUpdate("UPDATE gdo.Gruppen SET ZugriffAdministration = '"+ IsAdmin+"',ZugriffAdministrationLevel = '" + AdminLevel +"',ZugriffBewerber = '" + IsBewerber +"'"
                 + ",ZugriffBewerberLevel = '" + BewerberLevel +"',ZugriffDasboard = '" + IsDashboard +"',ZugriffDashboardLevel = '" + DashboardLevel +"' WHERE GruppenName = '" + GroupName +"'"); 
                               
         
        
         
         con.close();
    }  
      
      
      
           public void UpdateUserGroupDB (Integer groupid, String UserName) throws SQLException, NamingException {
    
        Connect_DB();
     
        st = con.createStatement();
        
         st.executeUpdate("UPDATE dbo.GD2Benutzer SET GruppenID = '"+groupid+"' WHERE BenutzerName = '"+ UserName +"'");
         
        
         
         con.close();
    }   
      
      
      
      
            public void InsertGroupDB (Integer groupid, String GroupName, Boolean IsAdmin, Integer AdminLevel,Boolean IsDashboard,  Integer DashboardLevel, Boolean IsBewerber,
                                        Integer BewerberLevel) throws SQLException, NamingException {
    
        Connect_DB();
     
        st = con.createStatement();
        
         st.executeUpdate("INSERT INTO dbo.GD2Gruppen(GruppenID,GruppenName,ZugriffAdministration,ZugriffAdministrationLevel,ZugriffDashboard,ZugriffDashboardLevel,ZugriffBewerber,ZugriffBewerberLevel)" +
                               "VALUES('"+groupid+"','" + GroupName +"','" + IsAdmin +"','" + AdminLevel +"','" + IsDashboard +"','" + DashboardLevel +"','" + IsBewerber +"','" + BewerberLevel +"')");
         
        
         
         con.close();
    }    
            
            
              public void RemoveGroupDB (String groupName) throws SQLException, NamingException {
    
        Connect_DB();
     
        st = con.createStatement();
        
         st.executeUpdate("DELETE FROM dbo.GD2Gruppen WHERE GruppenName = '"+ groupName +"'");
         
        
         
         con.close();
    }     
              
              
          public void RemoveUserDB (String userName) throws SQLException, NamingException {
    
        Connect_DB();
     
        st = con.createStatement();
        
         st.executeUpdate("DELETE FROM dbo.GD2Benutzer WHERE BenutzerName = '"+ userName +"'");
         
        
         
         con.close();
    }           
      
      
          
          
          
          
    
}