/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.user;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author temp
 */
public class GD2_User_TreeDatasource {
    
   
    IndexedContainer users = new IndexedContainer();
    public transient Connection con = null;
    public Statement st = null;
    public ResultSet rs = null;
    public ResultSet rs2 = null;
    
    
    
    public IndexedContainer getTreeUsers () throws SQLException, NamingException {
        
        
        users.addContainerProperty("Group", String.class, null);
        users.addContainerProperty("GroupID", Integer.class, null);
        users.addContainerProperty("Username", String.class, null);
   
    
    Connect_DB();
    
     st = con.createStatement();
        
         rs = st.executeQuery("SELECT GruppenID,GruppenName FROM dbo.GD2Gruppen");

         
         
         while (rs.next()) {
         
             Integer groupid = rs.getInt("GruppenID");
             String groupname = rs.getString("GruppenName");
               
                 st = con.createStatement();
        
                 rs2 = st.executeQuery("SELECT * FROM dbo.GD2Benutzer WHERE GruppenID = '"+ groupid +  "'");

               while (rs2.next()) {
               
                   Item newItem = users.getItem(users.addItem());
                   newItem.getItemProperty("Group").setValue(groupname);
                   newItem.getItemProperty("GroupID").setValue(groupid);
                   newItem.getItemProperty("Username").setValue(rs2.getString("BenutzerName"));
               }
    
    

    }
    System.out.println(users);
    return users;
    
    }
    
    
     public Connection Connect_DB() throws SQLException, NamingException {
        
    Context initContext = new InitialContext();
    Context envContext  = (Context)initContext.lookup("java:/comp/env");
    DataSource ds = (DataSource)envContext.lookup("jdbc/GD2_MSSQL");
    con = ds.getConnection();
    return con;
}
    
    
    
}
