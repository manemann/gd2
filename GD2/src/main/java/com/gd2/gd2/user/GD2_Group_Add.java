/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.user;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Page;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;

/**
 *
 * @author temp
 */
public class GD2_Group_Add {
    public Window AddGroup = new Window("Gruppe Hinzufügen");
    
    public VerticalLayout AddGroupLayout = new VerticalLayout();
    public HorizontalLayout ButtonLayout = new HorizontalLayout();
    public VerticalLayout AddGroupTab1 = new VerticalLayout();
    public VerticalLayout AddGroupTab2 = new VerticalLayout();
    
    public Accordion AddGroupAccordion = new Accordion();
    
    public static TextField GroupName = new TextField("Gruppenname");
    
    
    public CheckBox IsAdmin = new CheckBox("Admin Zugriff");
    public OptionGroup admin_level = new OptionGroup("Zugriff");
    public CheckBox IsDashboard = new CheckBox("Dashboard Zugriff");
    public OptionGroup dashboard_level = new OptionGroup("Zugriff");
    public CheckBox IsBewerber = new CheckBox("Bewerber Zugriff");
    public OptionGroup bewerber_level = new OptionGroup("Zugriff");
    
    public Integer AdminLevel = 0;
    public Integer BewerberLevel = 0;
    public Integer DashboardLevel = 0;
    
    public Button Save = new Button("Speichern");
    public Button Close = new Button("Schliessen");
    
    
    
    
    
    
    
    
    
    
    
    public Window AddGroup () throws SQLException, NamingException {
        
        
        
          
        FormLayout details3 = new FormLayout();
        details3.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
        details3.setMargin(true);
        details3.setSpacing(true);
        

        AddGroupLayout.addStyleName("profile-form");
        
        AddGroupLayout.addComponent(details3);
        AddGroupLayout.setExpandRatio(details3, 1);
        AddGroupLayout.setComponentAlignment(details3, Alignment.TOP_RIGHT);
        AddGroupLayout.setMargin(true);
        AddGroupLayout.setSpacing(true);
        
        Save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        Close.addStyleName(ValoTheme.BUTTON_PRIMARY);
        
        
        admin_level.addItem("voll");
        admin_level.addItem("bearbeiten");
        admin_level.addItem("sehen");
        dashboard_level.addItem("voll");
        dashboard_level.addItem("bearbeiten");
        dashboard_level.addItem("sehen");
        bewerber_level.addItem("voll");
        bewerber_level.addItem("bearbeiten");
        bewerber_level.addItem("sehen");
        
        admin_level.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
        dashboard_level.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
        bewerber_level.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);

        details3.addComponent(GroupName);
        details3.addComponent(IsAdmin);
        details3.addComponent(admin_level);
        details3.addComponent(IsDashboard);
        details3.addComponent(dashboard_level);
        details3.addComponent(IsBewerber);
        details3.addComponent(bewerber_level);



        ButtonLayout.addComponent(Save);
        

        
        GroupName.setImmediate(true);
        
        Save.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                    
                               if(admin_level.getValue() == "voll"){
                               AdminLevel = 1;
                               }
                               if(admin_level.getValue() == "bearbeiten"){
                               AdminLevel = 2;
                               }
                               if(admin_level.getValue() == "sehen"){
                               AdminLevel = 3;
                               }
                               if(bewerber_level.getValue() == "voll"){
                               BewerberLevel = 1;
                               }
                               if(bewerber_level.getValue() == "bearbeiten"){
                               BewerberLevel = 2;
                               }
                               if(bewerber_level.getValue() == "sehen"){
                               BewerberLevel = 3;
                               }
                               if(dashboard_level.getValue() == "voll"){
                               DashboardLevel = 1;
                               }
                               if(dashboard_level.getValue() == "bearbeiten"){
                               DashboardLevel = 2;
                               }
                               if(dashboard_level.getValue() == "sehen"){
                               DashboardLevel = 3;
                               }
                    
                    
               GD2_Users_Datasource addGroup = new GD2_Users_Datasource();
                    try {
                        addGroup.Exec_query_group_groupid_latest();
                        addGroup.InsertGroupDB(addGroup.gid_latest + 1, GroupName.getValue(), IsAdmin.getValue(),AdminLevel,IsDashboard.getValue(),DashboardLevel,IsBewerber.getValue(),BewerberLevel);
                    } catch (SQLException ex) {
                        Logger.getLogger(GD2_Group_Add.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NamingException ex) {
                        Logger.getLogger(GD2_Group_Add.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    
             
                     
               AddGroup.close();
               GroupName.setValue("");
               
               Page.getCurrent().reload();
    
                    
                Notification.show("Gruppe Hinzugefügt",Notification.TYPE_HUMANIZED_MESSAGE);
                    
                         
         GD2_User_TreeDatasource tree = new GD2_User_TreeDatasource();
         
                    try {
                        tree.getTreeUsers();
                    } catch (SQLException ex) {
                        Logger.getLogger(GD2_Group_Add.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NamingException ex) {
                        Logger.getLogger(GD2_Group_Add.class.getName()).log(Level.SEVERE, null, ex);
                    }
         
        
                
                    
                
                }});
        
        
               Close.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                
               AddGroup.close();
               GroupName.setValue("");
   
                
                }});
        
        
        
        
        
        
        ButtonLayout.addComponent(Close);
        ButtonLayout.setMargin(true);
        ButtonLayout.setSpacing(true);
       
        
        AddGroupLayout.addComponent(ButtonLayout);
        
        
        AddGroupLayout.setMargin(true);
        AddGroupLayout.setSpacing(true);
    
        AddGroup.setContent(AddGroupLayout);
        AddGroup.setHeight(70, Sizeable.Unit.PERCENTAGE);
        AddGroup.setWidth(40, Sizeable.Unit.PERCENTAGE);
       
        
    return AddGroup;
    }
    
}
