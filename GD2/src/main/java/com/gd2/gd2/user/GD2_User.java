/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.user;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.event.Transferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.server.Page;
import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Tree;
import com.vaadin.ui.Tree.TreeDragMode;
import com.vaadin.ui.Tree.TreeTargetDetails;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;

/**
 *
 * @author temp
 */

    public class GD2_User {
        
         public VerticalLayout user_layout = new VerticalLayout();
         public VerticalLayout user_details_layout = new VerticalLayout();
         public HorizontalLayout first_row = new HorizontalLayout();
         public HorizontalLayout second_row = new HorizontalLayout();
         public HorizontalLayout details_first_row = new HorizontalLayout();
         public HorizontalLayout details_second_row = new HorizontalLayout();
         public Tree users_tree = new Tree("Gespeicherte Gruppen/Nutzer");
         public Button addUser = new Button("Nutzer Hinzufügen");
         public Button addGroup = new Button("Gruppe Hinzufügen");
         public Button update = new Button("Datensatz aktualisieren");
         public Button delete = new Button("Datensatz Löschen");
         public PasswordField password = new PasswordField("Password");
         public TextField firstname = new TextField("Vorname");
         public TextField lastname = new TextField("Nachname");
         public TextField email = new TextField("Email");
         public TextField phone = new TextField("Telefon");
         public ComboBox site = new ComboBox("Niederlassung");
         public TextField PNummer = new TextField("Personal Nummer");
         public Label groupname = new Label();
         public CheckBox is_admin = new CheckBox("Admin Zugriff");
         public OptionGroup admin_level = new OptionGroup("Zugriff");
         public CheckBox is_dashboard = new CheckBox("Dashboard Zugriff");
         public OptionGroup dashboard_level = new OptionGroup("Zugriff");
         public CheckBox is_bewerber = new CheckBox("Bewerber Zugriff");
         public OptionGroup bewerber_level = new OptionGroup("Zugriff");
         public ArrayList<String> groups = new ArrayList<String>();
         public Integer AdminLevel = 0;
         public Integer BewerberLevel =0;
         public Integer DashboardLevel = 0;
         
         
         
         public VerticalLayout get_user_layout () throws SQLException, NamingException {
             addUser.addStyleName(ValoTheme.BUTTON_PRIMARY);
             addGroup.addStyleName(ValoTheme.BUTTON_PRIMARY);
             update.addStyleName(ValoTheme.BUTTON_FRIENDLY);
             delete.addStyleName(ValoTheme.BUTTON_DANGER);
         user_layout.setImmediate(true);
             
        user_layout.setSizeFull();
        user_details_layout.setSizeFull();
        
        second_row.setSizeFull();
        
        details_first_row.setSizeFull();
        details_second_row.setSizeFull();
        get_user_tree(); 

             
        FormLayout details3 = new FormLayout();
        details3.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
        details3.setMargin(true);
        details3.setSpacing(true);
        
        GD2_Users_Datasource sites = new GD2_Users_Datasource();
        sites.Exec_query_sites();
        
        for (int i = 0; i < sites.sites.size(); i++) {
		site.addItem(sites.sites.get(i));}
        
        

        groupname.addStyleName(ValoTheme.LABEL_H4);
        groupname.addStyleName(ValoTheme.LABEL_COLORED); 
        groupname.addStyleName(ValoTheme.TEXTFIELD_BORDERLESS);
        groupname.setEnabled(false);
         
         
        
        
        users_tree.addValueChangeListener(new ValueChangeListener(){
            
               @Override
             public void valueChange(Property.ValueChangeEvent event) {
                 
                 
                    // first_row.addComponent(groupname);
                     

                     first_row.addComponent(update);
                     
                     
                    
                     first_row.addComponent(delete);
                     
                     
                
                     
                     
                     
                 
                 if (null == users_tree.getValue()){
                 
                 Notification.show("Nichts ausgewÃ¤hlt");
                 }
            
                 else if (groups.contains(users_tree.getValue().toString())){
                     
                 
                     
                     
                     details3.removeComponent(password);
                     details3.removeComponent(firstname);
                     details3.removeComponent(lastname);
                     details3.removeComponent(PNummer);
                     
                     details3.removeComponent(email);
                     details3.removeComponent(phone);
                     details3.removeComponent(site);
                     
                     
                    GD2_Users_Datasource group_details = new GD2_Users_Datasource();
                    
                    
                     
                     try {
                         group_details.Exec_query_group_groupname(users_tree.getValue().toString());
                     } catch (SQLException ex) {
                         Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                     } catch (NamingException ex) {
                         Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                     }
                     
                     groupname.setValue(group_details.group_name);
                     is_admin.setValue(group_details.is_admin);
      
                     is_bewerber.setValue(group_details.is_bewerber);
                     
                     details3.addComponent(groupname);
                     details3.addComponent(is_admin);
                     details3.addComponent(admin_level);
                     details3.addComponent(is_bewerber);
                     details3.addComponent(bewerber_level);
                     details3.addComponent(is_dashboard);
                     details3.addComponent(dashboard_level);
                     
                     
                     //details_first_row.setSpacing(true);
                     
                     
                     //details_second_row.setSpacing(true);
                     
                     
                    // user_details_layout.addComponent(details_first_row);
                    // user_details_layout.addComponent(details_second_row);
                     
                     //second_row.addComponent(user_details_layout);
                     //second_row.setExpandRatio(user_details_layout, 5);
                     
                 
                 }
                 
                 else {
                     
                     GD2_Users_Datasource user_details = new GD2_Users_Datasource();
                     
                     try {
                         user_details.Exec_query_users(users_tree.getValue().toString());
                     } catch (SQLException ex) {
                         Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                     } catch (NamingException ex) {
                         Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                     }
                     
                     try {
                         user_details.Exec_query_site();
                     } catch (SQLException ex) {
                         Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                     } catch (NamingException ex) {
                         Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                     }
                     
                     
                     details3.removeComponent(groupname);
                     details3.removeComponent(is_admin);                
                     details3.removeComponent(admin_level);
                     details3.removeComponent(is_bewerber);
                     details3.removeComponent(bewerber_level);
                     details3.removeComponent(is_dashboard);
                     details3.removeComponent(dashboard_level);
                     
                     
                     groupname.setValue(user_details.username);
                     
                     password.setValue(user_details.password);
                     firstname.setValue(user_details.firstname);
                     lastname.setValue(user_details.lastname);
                     PNummer.setValue(user_details.PNummer.toString());
                     email.setValue(user_details.email);
                     phone.setValue(user_details.phone);
                      site.setValue(user_details.niederlassung_name);
                     
                     details3.addComponent(groupname);
                     details3.addComponent(password);
                     details3.addComponent(firstname);
                     details3.addComponent(lastname);
                     details3.addComponent(PNummer);
                     //details_first_row.setSpacing(true);
                     details3.addComponent(email);
                     details3.addComponent(phone);
                     details3.addComponent(site);
//                     details_second_row.setSpacing(true);
                    
                     
                     //user_details_layout.addComponent(details_first_row);
                     //user_details_layout.addComponent(details_second_row);
                     
                    // second_row.addComponent(user_details_layout);
                    // second_row.setExpandRatio(user_details_layout, 5);
                     
                     
                     
                     
                 }
                 
                 
                 
                 }
        });
        
        
        
        
        
         
         
         
         
         //users_tree.setSizeFull();
        
         
         first_row.addComponent(addUser);
         
         
         addUser.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                
                GD2_User_Add add = new GD2_User_Add();
                
                    try {
                        add.AddUser();
                    } catch (SQLException ex) {
                        Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NamingException ex) {
                        Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
                UI ui = UI.getCurrent();
                ui.addWindow(add.AddUser);
                add.AddUser.center();
                
                
                
                }});
         
         
         
         
         
         first_row.addComponent(addGroup);
         
         addGroup.addClickListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent event) {
                
                
                    GD2_Group_Add group = new GD2_Group_Add();
                    
                    try {
                        group.AddGroup();
                    } catch (SQLException ex) {
                        Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NamingException ex) {
                        Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
                 UI ui = UI.getCurrent();
                 ui.addWindow(group.AddGroup);
                 group.AddGroup.center();
                
                }});
         
          update.addClickListener(new Button.ClickListener(){
                      public void buttonClick(Button.ClickEvent event) {
                          
                          System.out.println(users_tree.hasChildren(users_tree.getValue()));
                          
                          
                          
                          
                          GD2_Group_Update update_group = new GD2_Group_Update();
                           if (users_tree.hasChildren(users_tree.getValue()) == true) {
                               
                               if(admin_level.getId() == "voll"){
                               AdminLevel = 1;
                               }
                               if(admin_level.getId() == "bearbeiten"){
                               AdminLevel = 2;
                               }
                               if(admin_level.getId() == "sehen"){
                               AdminLevel = 3;
                               }
                               if(bewerber_level.getId() == "voll"){
                               BewerberLevel = 1;
                               }
                               if(bewerber_level.getId() == "bearbeiten"){
                               BewerberLevel = 2;
                               }
                               if(bewerber_level.getId() == "sehen"){
                               BewerberLevel = 3;
                               }
                               if(dashboard_level.getId() == "voll"){
                               DashboardLevel = 1;
                               }
                               if(dashboard_level.getId() == "bearbeiten"){
                               DashboardLevel = 2;
                               }
                               if(dashboard_level.getId() == "sehen"){
                               DashboardLevel = 3;
                               }
                               
                     
                               
                               
              
                          
                          try {
                              update_group.UpdateGroup(users_tree.getValue().toString(),is_admin.getValue(),AdminLevel,is_bewerber.getValue(),BewerberLevel,is_dashboard.getValue(),DashboardLevel);
                                                  
                          } catch (SQLException ex) {
                              Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                          } catch (NamingException ex) {
                              Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                          }
                          
                     UI ui = UI.getCurrent();
                     ui.addWindow(update_group.UpdateGroup);
                     update_group.UpdateGroup.center(); 
                               
                               
                           }
                           
                           GD2_User_Update update_user = new GD2_User_Update();
                          if (users_tree.hasChildren(users_tree.getValue()) == false) {
                     
                          
                        
                          try {
                              
                              
                              update_user.UpdateUser(users_tree.getValue().toString(), groupname.getValue(),password.getValue() , firstname.getValue(), lastname.getValue()
                                      , phone.getValue(), email.getValue(), site.getTabIndex(),Integer.valueOf(PNummer.getValue()));
                          } catch (SQLException ex) {
                              Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                          } catch (NamingException ex) {
                              Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                          }
                     UI ui = UI.getCurrent();
                     ui.addWindow(update_user.UpdateUser);
                     update_user.UpdateUser.center();}
                          }});
         
          
                  delete.addClickListener(new Button.ClickListener(){
                      public void buttonClick(Button.ClickEvent event) {
                          
                      
                          
                          
                          
                          
                          GD2_Group_Delete delete_group = new GD2_Group_Delete();
                           if (users_tree.hasChildren(users_tree.getValue()) == true) {
              
                          
                          try {
                              delete_group.DeleteGroup(users_tree.getValue().toString());
                                                  
                          } catch (SQLException ex) {
                              Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                          } catch (NamingException ex) {
                              Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                          }
                          
                     UI ui = UI.getCurrent();
                     ui.addWindow(delete_group.DeleteGroup);
                     delete_group.DeleteGroup.center(); 
                               
                               
                           }
                           
                           GD2_User_Delete delete_user = new GD2_User_Delete();
                          if (users_tree.hasChildren(users_tree.getValue()) == false) {
                     
                          
                        
                          try {
                              
                              
                              delete_user.DeleteUser(users_tree.getValue().toString());
                          } catch (SQLException ex) {
                              Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                          } catch (NamingException ex) {
                              Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
                          }
                     UI ui = UI.getCurrent();
                     ui.addWindow(delete_user.DeleteUser);
                     delete_user.DeleteUser.center();}
                          }});  
          
          
          
          
         
         first_row.setMargin(true);
         first_row.setSpacing(true);
         
         
        
         second_row.setMargin(true);
         second_row.setSpacing(true);
         user_layout.addComponent(first_row);
         user_layout.addComponent(second_row);
         user_layout.setImmediate(true);
         user_layout.setExpandRatio(first_row, 0.1f);
         user_layout.setExpandRatio(second_row, 0.9f);
//         user_details_layout.setImmediate(true);
//         user_details_layout.setMargin(true);
//         user_details_layout.setSpacing(true);
         
         second_row.setExpandRatio(users_tree, 0.3f);
         
        second_row.addStyleName("profile-form");
        
        second_row.addComponent(details3);
        second_row.setExpandRatio(details3, 0.7f);
        second_row.setComponentAlignment(details3, Alignment.TOP_RIGHT);
        second_row.setMargin(true);
        second_row.setSpacing(true);
        second_row.setWidth(100.0f, Sizeable.Unit.PERCENTAGE);
         second_row.setImmediate(true);
         users_tree.setImmediate(true);
         
         
         
         return user_layout;
         }
        
        
        public Tree get_user_tree (){
            
        
        GD2_User_TreeDatasource tree = new GD2_User_TreeDatasource();
        
         
             try {
                 tree.getTreeUsers();
             } catch (SQLException ex) {
                 Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
             } catch (NamingException ex) {
                 Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
             }
         
        List itemids = tree.users.getItemIds();
   
        
        for (int i = 0; i < itemids.size(); i++) {
            
           users_tree.addItem(tree.users.getContainerProperty(itemids.get(i), "Group").getValue());
           
           
          groups.add(tree.users.getContainerProperty(itemids.get(i), "Group").getValue().toString());
		    
           users_tree.addItem(tree.users.getContainerProperty(itemids.get(i), "Username").getValue());
           
           users_tree.setParent(tree.users.getContainerProperty(itemids.get(i), "Username").getValue(), tree.users.getContainerProperty(itemids.get(i), "Group").getValue());
        
           users_tree.setChildrenAllowed(tree.users.getContainerProperty(itemids.get(i), "Username").getValue(), false);
        }
        
        second_row.addComponent(users_tree);
        
        
        // Set the tree in drag source mode
        users_tree.setDragMode(TreeDragMode.NODE);

            // Allow the tree to receive drag drops and handle them
        users_tree.setDropHandler(new DropHandler() {
        public AcceptCriterion getAcceptCriterion() {
        return AcceptAll.get();
             }

    public void drop(DragAndDropEvent event) {
        // Wrapper for the object that is dragged
        Transferable t = event.getTransferable();

        // Make sure the drag source is the same tree
        if (t.getSourceComponent() != users_tree)
            return;

        TreeTargetDetails target = (TreeTargetDetails)
            event.getTargetDetails();

        // Get ids of the dragged item and the target item
        Object sourceItemId = t.getData("itemId");
        Object targetItemId = target.getItemIdOver();

        // On which side of the target the item was dropped
        VerticalDropLocation location = target.getDropLocation();

        HierarchicalContainer container = (HierarchicalContainer)
                users_tree.getContainerDataSource();

        // Drop right on an item -> make it a child
        if (location == VerticalDropLocation.MIDDLE){
            users_tree.setParent(sourceItemId, targetItemId);
            String newgroup = users_tree.getItemCaption(targetItemId);
            String transfered = users_tree.getItemCaption(t.getData("itemId"));
            GD2_Users_Datasource groupid = new GD2_Users_Datasource();
            
            try {
                groupid.Exec_query_group_groupid(newgroup);
                System.out.println(newgroup + transfered + groupid.gid);
                groupid.UpdateUserGroupDB(groupid.gid, transfered);
            } catch (SQLException ex) {
                Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NamingException ex) {
                Logger.getLogger(GD2_User.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
            
            
        }
        // Drop at the top of a subtree -> make it previous
        else if (location == VerticalDropLocation.TOP) {
            Object parentId = container.getParent(targetItemId);
            container.setParent(sourceItemId, parentId);
            container.moveAfterSibling(sourceItemId, targetItemId);
            container.moveAfterSibling(targetItemId, sourceItemId);
        }

        // Drop below another item -> make it next
        else if (location == VerticalDropLocation.BOTTOM) {
            Object parentId = container.getParent(targetItemId);
            container.setParent(sourceItemId, parentId);
            container.moveAfterSibling(sourceItemId, targetItemId);
        }
    }});

          
        
        
        return users_tree;
        }
        
        
    
}
