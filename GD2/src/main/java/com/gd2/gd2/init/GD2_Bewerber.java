/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.init;

import com.gd2.gd2.bewerber.GD2_Bewerber_add;
import com.gd2.gd2.kunden.GD2_Kunden_add;
import com.gd2.gd2.bewerber.GD2_Bewerber_datasource;
import com.gd2.gd2.bewerber.GD2_Bewerber_details;
import com.gd2.gd2.bewerber.GD2_Bewerber_edit;
import com.gd2.gd2.bewerber.GD2_Bewerber_editor;
import com.gd2.gd2.kunden.GD2_Kunden_details;
import com.gd2.gd2.kunden.GD2_Kunden_edit;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.Sizeable;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Select;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;

/**
 *
 * @author temp
 */
public class GD2_Bewerber {
    
    public VerticalLayout bewerber_main = new VerticalLayout();
    public HorizontalLayout bewerber_leftbar = new HorizontalLayout();
    public VerticalLayout bewerber_rightbar = new VerticalLayout();
    public String filter_string = "Nachname";
    public TextField search = new TextField("Suchen"); 
    
    public static BeanContainer<String, GD2_Bewerber_datasource> applicants = new BeanContainer<>(GD2_Bewerber_datasource.class);
    
    
 
      public VerticalLayout create_bw_layout() {
        
    
    search.setIcon(new ThemeResource("icon/glyphicons-28-search.png"));
    search.setDescription("Wenn kein Filter Ausgewählt wird Bewerber ID verwendet");
    
    
    final Button customer_details = new Button("Details");
    customer_details.setEnabled(false);
    customer_details.setWidthUndefined();
    customer_details.setWidth(80, Sizeable.Unit.PERCENTAGE);
   
    final Button customer_add = new Button("Neuer Bewerber");
    customer_add.setWidthUndefined();
    customer_add.setWidth(80, Sizeable.Unit.PERCENTAGE);
    final Button customer_edit = new Button("Details");
    customer_edit.setWidthUndefined();
    customer_edit.setWidth(80, Sizeable.Unit.PERCENTAGE);
    customer_edit.setEnabled(false);
     final Button customer_docs = new Button("Zusammenfassung erstellen");
     customer_docs.setWidthUndefined();
     customer_docs.setWidth(80, Sizeable.Unit.PERCENTAGE);
    
     
     
     
  //  customer_add.setEnabled(false);
  //  customer_edit.setEnabled(false);
    
  //  if ( VaadinSession.getCurrent().getAttribute("is_add_kunden").equals(true)) {
    
   
  //  customer_add.setEnabled(true);
  //  }
    
    
    
    
    
    final Grid customer_table = new Grid();
    customer_table.setContainerDataSource(applicants);
    customer_table.setSelectionMode(Grid.SelectionMode.MULTI);
    customer_table.setEditorEnabled(false);
    
    for (Column c : customer_table.getColumns()) {
        	c.setHidable(true);
    }
            
            
    customer_table.setColumnOrder("bewerberID","vorname", "nachname", "beruf","geburtsdatum","adresse","PLZ","ort","verantwStandort","erfasstVonPerson","erfasstVonStandort");
    

    customer_table.addItemClickListener(new ItemClickEvent.ItemClickListener() {
          
    
    @Override
    public void itemClick(ItemClickEvent event3){
        
        customer_details.setEnabled(true);
        customer_edit.setEnabled(true);
    
    //  if ( VaadinSession.getCurrent().getAttribute("is_modify_kunden").equals(true)) {
    
    customer_edit.setEnabled(true);
   
    

  //  }
 
    }} );
    
    
    
    
 
    
    
    customer_add.addClickListener(new Button.ClickListener(){
    
    public void buttonClick(Button.ClickEvent event){
    
           GD2_Bewerber_add add_window = new GD2_Bewerber_add();  
        try {
            add_window.getWindow();
        } catch (SQLException ex) {
            Logger.getLogger(GD2_Bewerber.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NamingException ex) {
            Logger.getLogger(GD2_Bewerber.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }

      
    });
    
    
    customer_edit.addClickListener(new Button.ClickListener(){
    
    public void buttonClick(Button.ClickEvent event){

       GD2_Bewerber_editor open_edit = new GD2_Bewerber_editor(); 
       open_edit.getWindow();
        
    }

      
    });
    
    customer_details.addClickListener(new Button.ClickListener(){
    
    public void buttonClick(Button.ClickEvent event){
    
        GD2_Bewerber_details details_window = new GD2_Bewerber_details();
        details_window.getWindow(customer_table.getSelectedRows());
    }

      
    });
    
    
    

applicants.setBeanIdProperty("bewerberID");


applicants.addBean(new GD2_Bewerber_datasource(2016110,"Sebastian", "Manemann", "sm", "Hoehenstraße 36a", "51491","Overath", "Deutschland","02204/7038470","0175/5857030","sebastian.manemann@gmail.com","20.01.1985","Deutsch","Software Entwickler", "Leverkusen", "Troisdorf", "hgrimm","21.03.2016"));

applicants.addBean(new GD2_Bewerber_datasource(2016112,"Karin", "Werner", "kw", "Erlöserkirchstraße 35", "50667","Köln", "Deutschland","02204/7038470","0175/5857030","karin.werner@telekom.de","24.01..1956","Deutsch","Kaufmän. Fachkraft", "Solingen", "Leverkusen", "hgrimm","26.03.2016"));

applicants.addBean(new GD2_Bewerber_datasource(2016111,"Elisabeth", "Manemann", "em", "Teststraße 36a", "6000","Leverkusen", "Deutschland","02204/7038470","0175/5857030","elisabeth.manemann@gmail.com","05.09..1986","Deutsch","Ba. Medienwirtschaft", "Neuss", "Neuss", "smanemann","24.03.2016"));
    
    
 
     search.addTextChangeListener(new FieldEvents.TextChangeListener() {    
         public void textChange(FieldEvents.TextChangeEvent event) { 
         applicants.removeContainerFilters(filter_string);


           if (! event.getText().isEmpty())
               applicants.addContainerFilter(new SimpleStringFilter(filter_string,event.getText(), true, false));

               
         }}); 
     
     
     final Select filter_select = new Select("Filter Auswählen:");
     
     filter_select.addItems(customer_table.getContainerDataSource().getContainerPropertyIds());

     filter_select.setNullSelectionAllowed(false);
     
     filter_select.addListener(new Property.ValueChangeListener() {

        
     
     public void valueChange(Property.ValueChangeEvent event2){
     
         applicants.setBeanIdProperty(filter_select.getValue());
         filter_string = (String) filter_select.getValue();
         
         
     
     
     }
     
     
     });
     
 

    

   customer_table.setSizeFull();
   
   HeaderRow filterRow = customer_table.appendHeaderRow();
   customer_table.setHeightMode(HeightMode.CSS);
   
   for (Object pid: customer_table.getContainerDataSource()
                     .getContainerPropertyIds()) {
    HeaderCell cell = filterRow.getCell(pid);

    // Have an input field to use for filter
    TextField filterField = new TextField();
    //filterField.setColumns();
       filterField.setSizeFull();
       filterField.setHeight(85, Sizeable.Unit.PERCENTAGE);
       filterField.setValue("Filter");
     //  filterField.setValue("Filter");
    // Update filter When the filter input is changed
    filterField.addTextChangeListener(change -> {
        // Can't modify filters so need to replace
        applicants.removeContainerFilters(pid);

        // (Re)create the filter if necessary
        if (! change.getText().isEmpty())
            applicants.addContainerFilter(
                new SimpleStringFilter(pid,
                    change.getText(), true, false));
    });
    cell.setComponent(filterField);
}
    
   customer_table.getColumn("bewerberID").setHidden(true);
   customer_table.getColumn("adresse").setHidden(true);
   customer_table.getColumn("geburtsdatum").setHidden(true);
   customer_table.getColumn("email").setHidden(true);
   customer_table.getColumn("kurzname").setHidden(true);
   customer_table.getColumn("land").setHidden(true);
   customer_table.getColumn("nationalitaet").setHidden(true);
   customer_table.getColumn("telefon").setHidden(true);
   customer_table.getColumn("mobilnr").setHidden(true);
    
    //bewerber_leftbar.addComponent(filter_select);
    //bewerber_leftbar.addComponent(search);
    //bewerber_leftbar.setComponentAlignment(search, Alignment.BOTTOM_RIGHT);
    //bewerber_leftbar.setComponentAlignment(filter_select, Alignment.BOTTOM_RIGHT);
    bewerber_leftbar.addComponent(customer_add);
    customer_add.setStyleName("primary");
    
    bewerber_leftbar.setComponentAlignment(customer_add, Alignment.BOTTOM_RIGHT);
    bewerber_leftbar.addComponent(customer_edit);
customer_edit.setStyleName("primary");

    bewerber_leftbar.setComponentAlignment(customer_edit, Alignment.BOTTOM_RIGHT);
//    bewerber_leftbar.addComponent(customer_details);
//    customer_details.setStyleName("primary");

//    bewerber_leftbar.setComponentAlignment(customer_details, Alignment.BOTTOM_RIGHT);
  //  bewerber_leftbar.addComponent(customer_docs);
    bewerber_leftbar.setWidth(48, Sizeable.Unit.PERCENTAGE);
  //  bewerber_leftbar.setComponentAlignment(customer_docs, Alignment.MIDDLE_LEFT);
    
    //bewerber_leftbar.setSpacing(true);
    bewerber_leftbar.setExpandRatio(customer_add, 1);
    bewerber_leftbar.setExpandRatio(customer_edit, 1);
//    bewerber_leftbar.setExpandRatio(customer_details, 1);
    
    
    bewerber_rightbar.addComponent(customer_table);
    bewerber_rightbar.setComponentAlignment(customer_table, Alignment.TOP_LEFT);
    bewerber_rightbar.setSizeFull();
    
    bewerber_main.addComponent(bewerber_leftbar);
    bewerber_main.addComponent(bewerber_rightbar);
    
    //bewerber_main.setHeight(80, Sizeable.Unit.PERCENTAGE);
    bewerber_main.setMargin(true);
    bewerber_main.setSpacing(true);
    bewerber_main.setExpandRatio(bewerber_leftbar, 0.15f);
    bewerber_main.setExpandRatio(bewerber_rightbar, 0.85f);
    bewerber_main.setSizeFull();

    
    return bewerber_main;
    }
    
    
    
    
    
    
    
    
}
