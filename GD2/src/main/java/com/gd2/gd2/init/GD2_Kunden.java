/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.init;

import com.gd2.gd2.kunden.GD2_Kunden_add;
import com.gd2.gd2.kunden.GD2_Kunden_details;
import com.gd2.gd2.kunden.GD2_Kunden_edit;
import com.gd2.gd2.kunden.GD2_Kunden_datasource;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Select;

/**
 *
 * @author temp
 */
public class GD2_Kunden {
    
    public HorizontalLayout kunden_main = new HorizontalLayout();
    public VerticalLayout kunden_leftbar = new VerticalLayout();
    public VerticalLayout kunden_rightbar = new VerticalLayout();
    public String filter_string = "firmenname";
    public TextField search = new TextField("Suchen"); 
    
    public static BeanContainer<String, GD2_Kunden_datasource> applicants = new BeanContainer<>(GD2_Kunden_datasource.class);
    
    
    
    public HorizontalLayout create_kd_layout() {
        
    
    search.setIcon(new ThemeResource("icon/glyphicons-28-search.png"));
    search.setDescription("Wenn kein Filter Ausgewählt gilt Firmenname");
    
    
    final Button customer_details = new Button("Details");
    customer_details.setEnabled(false);
    
    final Button customer_add = new Button("Neuer Kunde");
    final Button customer_edit = new Button("Kunde Bearbeiten");
  //  customer_add.setEnabled(false);
  //  customer_edit.setEnabled(false);
    
  //  if ( VaadinSession.getCurrent().getAttribute("is_add_kunden").equals(true)) {
    
   
  //  customer_add.setEnabled(true);
  //  }
    
    
    
    
    
    final Grid customer_table = new Grid();
    customer_table.setContainerDataSource(applicants);
    customer_table.setEditorEnabled(false);
    customer_table.setColumnOrder("firmenname", "ort", "branche");
    

    customer_table.addItemClickListener(new ItemClickListener() {
          
    
    @Override
    public void itemClick(ItemClickEvent event3){
        
        customer_details.setEnabled(true);
    
    //  if ( VaadinSession.getCurrent().getAttribute("is_modify_kunden").equals(true)) {
    
    customer_edit.setEnabled(true);
   
    

  //  }
 
    }} );
    
    
    
    
 
    
    
    customer_add.addClickListener(new Button.ClickListener(){
    
    public void buttonClick(ClickEvent event4){
    
     GD2_Kunden_add open_window = new GD2_Kunden_add();
     open_window.getWindow();
    
    }

      
    });
    
    
    customer_edit.addClickListener(new Button.ClickListener(){
    
    public void buttonClick(ClickEvent event5){
    
     GD2_Kunden_edit open_window = new GD2_Kunden_edit();
     open_window.getWindow(customer_table.getSelectedRow());
    
    }

      
    });
    
    customer_details.addClickListener(new Button.ClickListener(){
    
    public void buttonClick(ClickEvent event5){
    
     GD2_Kunden_details open_window = new GD2_Kunden_details();
     open_window.getWindow(customer_table.getSelectedRow());
    
    }

      
    });
    
    
    

applicants.setBeanIdProperty("firmenname");


applicants.addBean(new GD2_Kunden_datasource("Netorium GmbH","Bahnstraße 10", "65205", "Wiesbaden", "Media & Entertaiment", "Sebastian Manemann"));
applicants.addBean(new GD2_Kunden_datasource("Hensseler IT", "Höhenstraße 35", "51491", "Overath", "IT", "Dirk Hensseler"));
applicants.addBean(new GD2_Kunden_datasource("Deutsche Bahn AG","Bahnhofsplatz 1", "1111", "Berlin", "Verspätungen", "Rüdiger Grube"));

    
    
 
     search.addTextChangeListener(new TextChangeListener() {    
         public void textChange(TextChangeEvent event) { 
         applicants.removeContainerFilters(filter_string);


           if (! event.getText().isEmpty())
               applicants.addContainerFilter(new SimpleStringFilter(filter_string,event.getText(), true, false));

               
         }}); 
     
     
     final Select filter_select = new Select("Filter Auswählen:");
     
     filter_select.addItems(customer_table.getContainerDataSource().getContainerPropertyIds());
     filter_select.removeItem("projekte");
     filter_select.setNullSelectionAllowed(false);
     
     filter_select.addListener(new Property.ValueChangeListener() {

        
     
     public void valueChange(Property.ValueChangeEvent event2){
     
         applicants.setBeanIdProperty(filter_select.getValue());
         filter_string = (String) filter_select.getValue();
         
         
     
     
     }
     
     
     });
     
 

    
    
    
    
    customer_table.removeColumn("projekte");
    customer_table.removeColumn("ansprechpartner");
    customer_table.removeColumn("PLZ");
    customer_table.removeColumn("anschrift");
    customer_table.setSizeFull();
    
    kunden_leftbar.addComponent(filter_select);
    kunden_leftbar.addComponent(search);
    kunden_leftbar.addComponent(customer_details);
    kunden_leftbar.addComponent(customer_add);
      kunden_leftbar.addComponent(customer_edit);
    kunden_leftbar.setComponentAlignment(filter_select, Alignment.TOP_LEFT);
    kunden_leftbar.setComponentAlignment(search, Alignment.TOP_LEFT);
    kunden_leftbar.setComponentAlignment(customer_details, Alignment.MIDDLE_LEFT);
    kunden_leftbar.setComponentAlignment(customer_add, Alignment.MIDDLE_LEFT);
    kunden_leftbar.setComponentAlignment(customer_edit, Alignment.MIDDLE_LEFT);
    kunden_leftbar.setMargin(true);
    kunden_leftbar.setSpacing(true);
    
    kunden_rightbar.addComponent(customer_table);
    kunden_rightbar.setComponentAlignment(customer_table, Alignment.TOP_LEFT);
    kunden_rightbar.setSizeFull();
    
    kunden_main.addComponent(kunden_leftbar);
    kunden_main.addComponent(kunden_rightbar);
    
    kunden_main.setMargin(true);
    kunden_main.setSpacing(true);
    kunden_main.setExpandRatio(kunden_leftbar, 1);
    kunden_main.setExpandRatio(kunden_rightbar, 5);
    kunden_main.setSizeFull();
    
    return kunden_main;
    }
    
}
