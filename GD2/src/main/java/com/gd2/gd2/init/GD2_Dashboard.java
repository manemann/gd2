/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.init;

import com.vaadin.server.Sizeable;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Calendar;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.calendar.event.BasicEvent;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author temp
 */
public class GD2_Dashboard {
    
    public VerticalLayout dashboard = new VerticalLayout(); 
    public HorizontalLayout row1 = new HorizontalLayout();
    public HorizontalLayout row2 = new HorizontalLayout();
    
    
    public TextField search = new TextField("Suchen");
    public Table kunden = new Table("Kunden");
    public Table projekte = new Table("Projekte");
    
    public Table aktivitäten = new Table("Aktivitäten");
    public Calendar termine = new Calendar("aktuelle Termine");
    
    
    
    public VerticalLayout create_db_layout() {
        
        kunden.setSelectable(true);
        projekte.setSelectable(true);
        aktivitäten.setSelectable(true);
        
      kunden.addContainerProperty("Kundennr.", String.class, null);
      kunden.addContainerProperty("Firmenname.", String.class, null);
      kunden.addContainerProperty("Ansprechpartner.", String.class, null);
      kunden.addContainerProperty("Aktuelle Projekte.", String.class, null);
      
      kunden.addItem(new Object[]{"1001","Netorium GmbH","Sebastian Manemann","WebApp"}, 1);
      kunden.addItem(new Object[]{"1002","Hennseler IT","Dirk Hennseler","IT"}, 2);
      kunden.addItem(new Object[]{"1003","AWB","Herr Müller","keine"}, 3);
      
      
      projekte.addContainerProperty("Projektnr.", String.class, null);
      projekte.addContainerProperty("Anzahl Ma.", String.class, null);
      projekte.addContainerProperty("Laufzeit.", String.class, null);
      
      projekte.addItem(new Object[]{"09","Web Applikation","Q3/16"}, 1);
      projekte.addItem(new Object[]{"17","VPN","Q1/16"}, 2);
      
      
      aktivitäten.addContainerProperty("Mitarbeiter", String.class, null);
      aktivitäten.addContainerProperty("Projekt", String.class, null);
      aktivitäten.addContainerProperty("Beschreibung", String.class, null);
      
      aktivitäten.addItem(new Object[]{"Sebastian Manemann","Web Applikation","Dummy erstellt"}, 1);
      aktivitäten.addItem(new Object[]{"Dirk Hennseler","VPN","Software Installiert"}, 2); 
      
      
      GregorianCalendar start = new GregorianCalendar();
      GregorianCalendar end   = new GregorianCalendar();
      start.setTime(new Date());
      end.setTime(new Date());
      end.add(GregorianCalendar.HOUR, 1);

      termine.addEvent(new BasicEvent("Projektvorstellung",
        "Projektvorstellung GD2 , VPNs",
        start.getTime(),end.getTime()));
      
      
            GregorianCalendar start2 = new GregorianCalendar();
      GregorianCalendar end2   = new GregorianCalendar();
      start2.setTime(new Date());
      start2.add(GregorianCalendar.HOUR, 2);
      end2.setTime(new Date());
      end2.add(GregorianCalendar.HOUR, 5);

      termine.addEvent(new BasicEvent("Anderer Termin",
        "Das ist ein anderer Termin",
        start2.getTime(),end2.getTime()));
      
      kunden.setWidth(95, Sizeable.Unit.PERCENTAGE);
      aktivitäten.setWidth(95, Sizeable.Unit.PERCENTAGE);
      dashboard.setMargin(true);
      dashboard.setSpacing(true);
      row1.setMargin(true);
      row2.setMargin(true);
      row1.setSpacing(true);
      row2.setSpacing(true);
    
    dashboard.setHeight(70, Sizeable.Unit.PERCENTAGE);
    dashboard.addComponent(search);
    search.setSizeFull();
    search.setIcon(new ThemeResource("icon/glyphicons-28-search.png"));
    
    row1.addComponent(kunden);
    row1.addComponent(projekte);
    

    projekte.setSizeFull();

    row2.addComponent(aktivitäten);
   
    row2.addComponent(termine);
    termine.setSizeFull();
    
    
    termine.setStartDate(new Date());
    termine.setEndDate(new Date());
    
    
    row1.setSizeFull();
    row2.setSizeFull();
    
    
    dashboard.addComponent(row1);
    dashboard.addComponent(row2);
    
    
    
    
    
    
    
    
        
    return dashboard;
}}
