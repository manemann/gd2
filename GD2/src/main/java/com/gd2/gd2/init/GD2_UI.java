package com.gd2.gd2.init;

import com.gd2.gd2.user.GD2_User;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;

/**
 *
 */
@Theme("valo")
@Widgetset("com.gd2.gd2.GD2Widgetset")



public class GD2_UI extends UI {
    
    public VerticalLayout main = new VerticalLayout();
    public HorizontalLayout headline= new HorizontalLayout();
    public HorizontalLayout menubar = new HorizontalLayout();
    public VerticalLayout footer = new VerticalLayout();
    public VerticalLayout calendar = new VerticalLayout();
    public VerticalLayout projekte = new VerticalLayout();
    public VerticalLayout auswertung = new VerticalLayout();
    public VerticalLayout nutzer = new VerticalLayout();
    public VerticalLayout administration = new VerticalLayout();
    public VerticalLayout bewerber = new VerticalLayout();
    public VerticalLayout dashboard = new VerticalLayout();
    public VerticalLayout kunden = new VerticalLayout();
    public Window loginWindow = new Window("Bitte Anmelden");
    Button login_button = new Button("Login");
    TextField uname = new TextField("Nutzername");
    PasswordField pass = new PasswordField("Password");
    Button logout = new Button("Logout");
    public static GD2_login check = new GD2_login();
    
    public GD2_Mitarbeiter get_ma = new GD2_Mitarbeiter();
    public GD2_Calendar get_cal = new GD2_Calendar();
    public GD2_Kunden get_kd = new GD2_Kunden();
    public GD2_User get_us = new GD2_User();
    public GD2_Bewerber get_bw = new GD2_Bewerber();
    
    

    @Override
    
    
    protected void init(VaadinRequest vaadinRequest) {
        
        if (VaadinSession.getCurrent().getAttribute("Login") != null) {
            
           // create_headline();
            
            
            
            try {
                create_menubar();
            } catch (SQLException ex) {
                Logger.getLogger(GD2_UI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NamingException ex) {
                Logger.getLogger(GD2_UI.class.getName()).log(Level.SEVERE, null, ex);
            }
            create_footer();
            
            main.setSizeFull();
            
            
            
           // main.addComponent(headline);
            main.addComponent(menubar);
            main.addComponent(footer);
            main.setComponentAlignment(menubar, Alignment.TOP_CENTER);
            main.setExpandRatio(menubar, 0.80f);
            main.setExpandRatio(footer, 0.20f);
            
            
     
           
      //     main.setExpandRatio(menubar, 1);
      //      main.setExpandRatio(footer, 20);
            setContent(main);
        }
        
        else {
            
            
            
            
            create_login();
            
            
            setContent(main);
            
            addWindow(loginWindow);
            login_button.setStyleName("friendly");
            
            login_button.addClickListener(new Button.ClickListener() {
                public void buttonClick(ClickEvent event) {
                    
                    
                    try {
                        check.validate_login(uname.getValue(), pass.getValue());
                    } catch (SQLException ex) {
                        Logger.getLogger(GD2_UI.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NamingException ex) {
                        Logger.getLogger(GD2_UI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    if(check.login == true){
                        
                        
                        

                        try {
                            create_menubar();
                        } catch (SQLException ex) {
                            Logger.getLogger(GD2_UI.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (NamingException ex) {
                            Logger.getLogger(GD2_UI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        create_footer();
                        loginWindow.close();
                        
                        main.setSizeFull();
                        
                        //main.addComponent(headline);
                        main.addComponent(menubar);
                        main.addComponent(footer);
                       
                        main.setComponentAlignment(menubar, Alignment.TOP_CENTER);
                        main.setExpandRatio(menubar, 0.80f);
                        main.setExpandRatio(footer, 0.20f);

                  //     main.setExpandRatio(menubar, 1);
                  //     main.setExpandRatio(footer, 20);
                    }
                
                    else {
                    
                       
                       Notification.show("Nutzername/Password falsch oder nicht bekannt");
                    
                    }
                    }
                
                
            });
            
            
            
            
            
        }
        
        logout.setStyleName("danger");
        logout.addClickListener(new Button.ClickListener() {
                public void buttonClick(ClickEvent event) {
                
                
               VaadinSession.getCurrent().setAttribute("Login", null);
               VaadinSession.getCurrent().setAttribute("User", null);;
               
               Page.getCurrent().reload();
                
                
                }});
                    
        
        
        
        
        
        
    }
    
 
                    
    
    public Window create_login() {
        
        
        
        HorizontalLayout window = new HorizontalLayout();
        VerticalLayout login_main = new VerticalLayout();
        FormLayout login = new FormLayout();
        VerticalLayout image = new VerticalLayout();
        
        loginWindow.setWidth(50, Unit.PERCENTAGE);
        loginWindow.setHeight(55, Unit.PERCENTAGE);
        loginWindow.setClosable(false);
        loginWindow.setResizable(false);
        login.setSizeFull();
        login.setMargin(true);
        login.setStyleName(ValoTheme.FORMLAYOUT_LIGHT);
        
        Label willkommen = new Label("Willkommen zu GD2 - Bitte melden Sie sich an.");
        
        login_main.addComponent(willkommen);
        login_main.addComponent(login);
        
        window.setHeight(90, Unit.PERCENTAGE);
        window.setWidth(90, Unit.PERCENTAGE);
        
        loginWindow.setContent(window);
        window.addComponent(login_main);
        window.addComponent(image);
        
        Embedded em = new Embedded("", new ThemeResource("images/grimm.png"));
        
        
        image.addComponent(em);
        em.setSizeFull();
        
        image.setHeight(90, Unit.PERCENTAGE);
        image.setWidth(90, Unit.PERCENTAGE);
        
        image.setComponentAlignment(em, Alignment.MIDDLE_CENTER);
        
        window.setComponentAlignment(image, Alignment.MIDDLE_CENTER);
        
       
        

        
       
        login.addComponent(uname);
        login.addComponent(pass);
        
        
        login_main.addComponent(login_button);
        login_main.setMargin(true);
        login_main.setSpacing(true);
       
        loginWindow.center();
    
       return loginWindow;
    }
    
    public HorizontalLayout create_headline() {

       headline.setWidth(100, Unit.PERCENTAGE);
       //headline.setMargin(true);
       headline.setSpacing(true);
      
       
       
       Embedded em = new Embedded("", new ThemeResource("images/grimm.png"));
       
       
       
       headline.addComponent(em);
       headline.setComponentAlignment(em, Alignment.TOP_CENTER);
        
       
       
      
       return headline;
    }
    
       public HorizontalLayout create_menubar() throws SQLException, NamingException {
      
       menubar.setWidth(100, Unit.PERCENTAGE);
       menubar.setHeight(100, Unit.PERCENTAGE);
      
       
       menubar.setMargin(true);
       menubar.setSpacing(true);
       
       TabSheet menu = new TabSheet();
       menu.setSizeFull();
       GD2_Dashboard get_db = new GD2_Dashboard();
       get_db.create_db_layout();
       
    //   menu.addTab(get_db.dashboard, "Dashboard", new ThemeResource("icon/glyphicons-332-dashboard.png"));
       
       
     
       if (VaadinSession.getCurrent().getAttribute("is_bewerber").equals(true)) {
       
      get_bw.create_bw_layout();
       
       menu.addTab(get_bw.bewerber_main, "Bewerber", new ThemeResource("icon/glyphicons-7-user-add.png"));
       
             }
       
       

       
       
   //   get_ma.create_ma_layout();
       
    //   menu.addTab(get_ma.mitarbeiter, "Mitarbeiter", new ThemeResource("icon/glyphicons-44-group.png"));
       
       
       

       
      
 //      get_kd.create_kd_layout();
       
  //     menu.addTab(get_kd.kunden_main, "Kunden", new ThemeResource("icon/glyphicons-265-vcard.png"));
       

       
  
       
       
  //     menu.addTab(projekte, "Projekte", new ThemeResource("icon/glyphicons-378-riflescope.png"));
       
       
       
       

       
       
   //    get_cal.create_cal_layout();
       
   //    menu.addTab(get_cal.calendar, "Kalender", new ThemeResource("icon/glyphicons-46-calendar.png"));
       
       
       
      
           
           
       
  //     menu.addTab(auswertung, "Auswertung", new ThemeResource("icon/glyphicons-42-charts.png"));

       
       
       
       if (VaadinSession.getCurrent().getAttribute("is_admin").equals(true)) {
       
       
       get_us.get_user_layout();
       
       menu.addTab(get_us.user_layout, "User", new ThemeResource("icon/glyphicons-4-user.png"));   
      
       }
       if (VaadinSession.getCurrent().getAttribute("is_admin").equals(true)) {
       
       menu.addTab(administration, "Administration", new ThemeResource("icon/glyphicons-138-cogwheels.png"));
       
       }
       
       
       
       
          
     
       
       

       menu.setWidth(100, Unit.PERCENTAGE);
      
      // menubar.addComponent(em);
      
       menubar.addComponent(menu);
       menubar.setComponentAlignment(menu, Alignment.TOP_CENTER);
;
       
      // menubar.setComponentAlignment(em, Alignment.TOP_CENTER);
       //menubar.setComponentAlignment(menu, Alignment.BOTTOM_LEFT);
      
       
       
       
       return menubar;
    
}
       
       
           public VerticalLayout create_footer() {

       footer.setWidth(100, Unit.PERCENTAGE);
       //headline.setMargin(true);
       footer.setSpacing(false);
       HorizontalLayout footer_hor = new HorizontalLayout();
         Embedded em = new Embedded("", new ThemeResource("images/grimm.png"));
       
       Label label1 = new Label ( "Grimm Personalservice GD2 -- Willkommen " + VaadinSession.getCurrent().getAttribute("User").toString());
       Label label2 = new Label ( "Development Stage, for testing purpose only!");
       Label label3 = new Label ();
       label3.setIcon(new ThemeResource("icon/glyphicons-399-copyright-mark.png") );
       label3.setCaption("Sebastian Manemann - 2016");
       footer.addComponent(new Label("<hr />",Label.CONTENT_XHTML));
       footer.addComponent(em);
       footer.addComponent(footer_hor);
       footer.addComponent(logout);
       footer_hor.addComponent(label1);
       //footer_hor.addComponent(label2);
       //footer_hor.addComponent(label3);
       
       em.setSizeUndefined();
       label1.setSizeUndefined();
       label2.setSizeUndefined();
       label3.setSizeUndefined();
       
     
       footer.setComponentAlignment(logout, Alignment.MIDDLE_CENTER);
       footer.setComponentAlignment(footer_hor, Alignment.MIDDLE_CENTER);
       //ooter.setComponentAlignment(label2, Alignment.MIDDLE_CENTER);
       //footer.setComponentAlignment(label3, Alignment.MIDDLE_CENTER);
       footer.setComponentAlignment(em, Alignment.MIDDLE_CENTER);
      
       
      
       return footer;
    }

    @WebServlet(urlPatterns = "/*", name = "GD2Servlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = GD2_UI.class, productionMode = false)
    public static class GD2Servlet extends VaadinServlet {
    }
}
