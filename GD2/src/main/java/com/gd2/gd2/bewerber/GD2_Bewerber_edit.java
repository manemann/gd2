/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.bewerber;

import static com.gd2.gd2.bewerber.GD2_Bewerber_details.lebenslauf_bean;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author temp
 */


public class GD2_Bewerber_edit {
    
    
    public Window add_window = new Window();
    VerticalLayout mainLayout = new VerticalLayout();
    TabSheet tabs = new TabSheet();
    
   public HorizontalLayout persDaten = new HorizontalLayout();
   public HorizontalLayout buttons = new HorizontalLayout();
   public VerticalLayout persDaten_row1 = new VerticalLayout();
   public VerticalLayout persDaten_row2 = new VerticalLayout();
   public VerticalLayout persDaten_row3 = new VerticalLayout();
   
   public VerticalLayout berufDaten = new VerticalLayout();
   public VerticalLayout zust = new VerticalLayout();
   public HorizontalLayout docs = new HorizontalLayout();
   public VerticalLayout docs_row1 = new VerticalLayout();
   public VerticalLayout docs_row2 = new VerticalLayout();
   public VerticalLayout aktionen = new VerticalLayout();
   public VerticalLayout notizen = new VerticalLayout();
   public VerticalLayout create_expose = new VerticalLayout();
   public VerticalLayout vertrag_layout = new VerticalLayout();
   public VerticalLayout bericht_layout = new VerticalLayout();
   public HorizontalLayout bericht_row1 = new HorizontalLayout();
   public HorizontalLayout bericht_row2 = new HorizontalLayout();
   public Button save2 = new Button("Speichern");
   public Button save = new Button ("Speichern");
   public Button exit = new Button ("schliessen");
    public Button hinzu = new Button("Hinzufügen");
   
   public TextField memo = new TextField("Notizen:");
   
     public static BeanContainer<String, GD2_BewerberLebenslauf_datasource> lebenslauf_bean = new BeanContainer<>(GD2_BewerberLebenslauf_datasource.class);
     public static BeanContainer<String, GD2_BewerberAktionen_datasource> aktionen_bean = new BeanContainer<>(GD2_BewerberAktionen_datasource.class);
     
     
   public Panel panel_docs = new Panel();
   public Panel panel_persDaten = new Panel();
   public Panel panel_berufDaten = new Panel();
   public Panel panel_zust = new Panel();
   public Panel panel_aktionen = new Panel();
   public Panel panel_expose = new Panel();
   public Panel panel_notizen = new Panel();
public Window getWindow() {
    
    
  save.setStyleName("friendly");
  exit.setStyleName("primary");

  tabs.addTab(panel_persDaten,"Persönliche Daten");
  tabs.addTab(panel_berufDaten,"Berufliche Daten");
  tabs.addTab(panel_zust,"Zuständigkeit");
  tabs.addTab(panel_docs,"Dokumente");
  tabs.addTab(panel_aktionen,"Aktionen");
  tabs.addTab(panel_expose, "Unterlagen");
  tabs.addTab(panel_notizen, "Notizen");
  tabs.setSizeFull();
  
  Label gesamtdaten = new Label("Gesamtdatensatz:");
  
   
   memo.setWidthUndefined();
   memo.setWidth(80, Sizeable.Unit.PERCENTAGE);
mainLayout.addComponent(tabs);

//mainLayout.addComponent(new Label("<hr />",Label.CONTENT_XHTML));
buttons.addComponent(gesamtdaten);
buttons.addComponent(save);
buttons.addComponent(exit);
buttons.setMargin(true);
buttons.setSpacing(true);

mainLayout.addComponent(buttons);

mainLayout.setExpandRatio(buttons, 0.1f);
mainLayout.setExpandRatio(tabs, 0.9f);

mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_LEFT);


mainLayout.setMargin(true);
mainLayout.setSpacing(true);
mainLayout.setSizeFull();



        add_window.setWidth(70, Sizeable.Unit.PERCENTAGE);
        add_window.setHeight(80, Sizeable.Unit.PERCENTAGE);
        add_window.center();
    
        UI ui = UI.getCurrent();
        add_window.setContent(mainLayout);
        ui.addWindow(add_window);





    
   


ComboBox titel = new ComboBox("Anrede:");
titel.addItem("Herr");
titel.addItem("Frau");
titel.setRequired(true);
titel.setValue("Herr");


TextField vorName = new TextField("Vorname:");
vorName.setRequired(true);
vorName.setValue("Sebastian");
TextField nachName = new TextField("Nachname:");
nachName.setValue("Manemann");
nachName.setRequired(true);
TextField kurzName = new TextField("Kurzname:");
kurzName.setValue("Smanemann");
kurzName.setRequired(true);
TextField Adresse = new TextField("Adresse:");
Adresse.setValue("Höhenstraße 36a");
TextField PLZ = new TextField("PLZ:");
PLZ.setValue("51491");
TextField Ort = new TextField("Ort:");
Ort.setValue("Overath");

vorName.setDescription("Bitte den Vornamen eingeben.");


persDaten_row1.addComponent(titel);
persDaten_row1.addComponent(vorName);
persDaten_row1.addComponent(nachName);
persDaten_row1.addComponent(kurzName);
persDaten_row1.addComponent(Adresse);
persDaten_row1.addComponent(PLZ);
persDaten_row1.addComponent(Ort);


persDaten_row1.setMargin(true);
persDaten_row1.setSpacing(true);



persDaten.addComponent(persDaten_row1);



TextField geburtsDatum = new TextField("Geburtsdatum:");
geburtsDatum.setValue("20.01.1985");

ComboBox fSchein = new ComboBox("Führerschein:");

fSchein.addItem("keine");
fSchein.addItem("Klaase A1");
fSchein.addItem("Klaase A");
fSchein.addItem("Klasse B");
fSchein.addItem("Klasse C1");
fSchein.addItem("Klasse C");
fSchein.addItem("Klasse D1");
fSchein.addItem("Klasse D");
fSchein.addItem("Klasse BE");
fSchein.addItem("Klasse C1E");
fSchein.addItem("Klasse CE");
fSchein.addItem("Klasse D1E");
fSchein.addItem("Klasse DE");
fSchein.addItem("Klasse M");
fSchein.addItem("Klasse L");
fSchein.setValue("Klasse B");
CheckBox fahrzeug = new CheckBox("Fahrzeug vorhanden");
fahrzeug.setValue(true);
TextField nationalitaet = new TextField("Nationalität:");
nationalitaet.setValue("Deutsch");


TextField email = new TextField("Email Adresse:");
email.setValue("sebastian.manemann@gmail.com");
email.setRequired(true);
TextField phone = new TextField ("Telefonnummer:");
phone.setValue("022047038470");
phone.setRequired(true);
TextField fax = new TextField ("Faxnummer:");

TextField mobile = new TextField("Mobilnummer:");
mobile.setValue("01755857030");
TextField website = new TextField ("Webseite:");
website.setValue("www.sebastian-manemann.de");
TextArea comment = new TextArea("Kommentare:");
comment.setValue("Bewerbung per Brieftaube");

persDaten_row2.addComponent(geburtsDatum);
persDaten_row2.addComponent(fSchein);
persDaten_row2.addComponent(fahrzeug);
persDaten_row2.addComponent(nationalitaet);

persDaten_row2.addComponent(phone);
persDaten_row2.addComponent(mobile);



persDaten_row2.setMargin(true);
persDaten_row2.setSpacing(true);

persDaten.addComponent(persDaten_row2);

persDaten_row3.addComponent(email);
persDaten_row3.addComponent(fax);
persDaten_row3.addComponent(website);
persDaten_row3.addComponent(comment);

persDaten_row3.setMargin(true);
persDaten_row3.setSpacing(true);


persDaten.addComponent(persDaten_row3);

panel_persDaten.setContent(persDaten);
panel_persDaten.setSizeFull();

ComboBox beruf = new ComboBox("erlernter Beruf:");

beruf.addItem("Software Entwickler");
beruf.addItem("Industriemechaniker");
beruf.addItem("Schlosser");
beruf.addItem("Maler & Lackierer");
beruf.setValue("Software Entwickler");


 final Grid lebenslauf_table = new Grid();
    lebenslauf_table.setContainerDataSource(lebenslauf_bean);
   lebenslauf_bean.setBeanIdProperty("vonJahr");
   lebenslauf_table.setColumnOrder("vonJahr","bisJahr","taetigkeit");
   lebenslauf_bean.addBean(new GD2_BewerberLebenslauf_datasource("2006", "2014", "Koordinator Transcoding - WDR mediagroup GmbH","","",false,""));
 
    lebenslauf_table.setEditorEnabled(true); 
    lebenslauf_table.setSizeUndefined();
    lebenslauf_table.setHeightUndefined();
   lebenslauf_table.setHeight(30, Sizeable.Unit.PERCENTAGE);
   lebenslauf_table.setWidth(100, Sizeable.Unit.PERCENTAGE);
   lebenslauf_table.setImmediate(true);
  
   
   
HorizontalLayout lebenslauf_layout = new HorizontalLayout();
final TextField datumVon = new TextField("Von Jahr:");
final TextField datumBis = new TextField("Bis Jahr:");
final TextField Taetigkeit = new TextField("Tätigkeit:");
datumVon.setWidthUndefined();
datumVon.setWidth(100, Sizeable.Unit.PERCENTAGE);
datumBis.setWidthUndefined();
datumBis.setWidth(100, Sizeable.Unit.PERCENTAGE);
Taetigkeit.setWidthUndefined();
Taetigkeit.setWidth(144, Sizeable.Unit.PERCENTAGE);
lebenslauf_layout.addComponent(datumVon);
lebenslauf_layout.addComponent(datumBis);
lebenslauf_layout.addComponent(Taetigkeit);
lebenslauf_layout.addComponent(hinzu);
lebenslauf_layout.setWidth(80, Sizeable.Unit.PERCENTAGE);
lebenslauf_layout.setComponentAlignment(hinzu, Alignment.BOTTOM_RIGHT);


hinzu.addClickListener(new Button.ClickListener(){
    
    public void buttonClick(Button.ClickEvent event){
    
//  lebenslauf_bean.addBean(new GD2_BewerberLebenslauf_datasource(datumVon.getValue(), datumBis.getValue(), Taetigkeit.getValue()));
    
    }

      
    });


//TextField ausgeBerufe = new TextField("Bisher ausgeübte Berufe:");
//ausgeBerufe.setDescription("Bitte mehere Berufe mit , trennen");
TextField sprachen = new TextField("Sprachen:");
sprachen.setValue("Deutsch,Englisch");
sprachen.setDescription("Bitte mehrere Sprachen mit , trennen");
TextField zusaetzlKentnisse = new TextField("Zusätzliche Kentnisse");
zusaetzlKentnisse.setValue("Projekt Manager IHK, IBM Sales Professional, IBM Aspera tech. Professional");

beruf.setWidthUndefined();
beruf.setWidth(40, Sizeable.Unit.PERCENTAGE);
Label lebenslauf_header = new Label("Lebenslauf:");


sprachen.setWidthUndefined();
sprachen.setWidth(70, Sizeable.Unit.PERCENTAGE);

zusaetzlKentnisse.setWidthUndefined();
zusaetzlKentnisse.setWidth(70, Sizeable.Unit.PERCENTAGE);

berufDaten.addComponent(beruf);
berufDaten.addComponent(lebenslauf_header);
berufDaten.addComponent(lebenslauf_layout);
berufDaten.addComponent(lebenslauf_table);
berufDaten.addComponent(sprachen);
berufDaten.addComponent(zusaetzlKentnisse);

berufDaten.setMargin(true);
berufDaten.setSpacing(true);

panel_berufDaten.setContent(berufDaten);
panel_berufDaten.setSizeFull();


//Label aktuellStandort = new Label("erfasst an Standort:");
Label Standort = new Label("Leverkusen - Hauptsitz");
Standort.setCaption("Erfasst von Niederlassung:");

//Label bewID = new Label("Bewerber ID:");
Label bewIDgen = new Label("2016011000");
bewIDgen.setCaption("Bewerber ID:");

ComboBox standort = new ComboBox("Niederlassung Zuweisen:");
standort.addItem("aktuelle Niederlassung");
standort.addItem("Leverkusen - Hauptsitz");
standort.addItem("Troisdorf");
standort.addItem("Düsseldorf");
standort.addItem("Mülheim a.d. Ruhr");
standort.addItem("Monheim");
standort.addItem("Neuss");
standort.addItem("Solingen");

standort.setValue("aktuelle Niederlassung");
DateField eingangDBW = new DateField("Eingang der Bewerbung:");
eingangDBW.setValue(new Date());
CheckBox verwenden = new CheckBox("Bewerber von mir verwendet");
verwenden.setValue(true);
CheckBox verfuegbar = new CheckBox("Bewerber ist Verfügbar");
verfuegbar.setValue(true);
  CheckBox isArchive = new CheckBox("Ist archiviert");


//zust.addComponent(bewID);
zust.addComponent(bewIDgen);
//zust.addComponent(aktuellStandort);
zust.addComponent(Standort);
zust.addComponent(standort);
//zust.addComponent(eingangDBW);
zust.addComponent(verwenden);
zust.addComponent(verfuegbar);
zust.addComponent(isArchive);

zust.setMargin(true);
zust.setSpacing(true);

panel_zust.setContent(zust);
panel_zust.setSizeFull();
        

Label lebenslauf_label = new Label("Wählen Sie den Lebenslauf aus:");
Upload lebenslauf = new Upload();

Label cover_label = new Label("Wählen Sie das Anschreiben aus:");
Upload cover = new Upload();

Label resume_label = new Label("Wählen Sie die Zeugnisse aus:");
Upload resume = new Upload();

Label profilbild_label = new Label("Wählen Sie ein Profilbild aus:");
Upload profilbild = new Upload();

Label more_label = new Label("Weitere Anhänge:");
Upload more = new Upload();


docs_row1.addComponent(lebenslauf_label);
docs_row1.addComponent(lebenslauf);

docs_row1.addComponent(cover_label);
docs_row1.addComponent(cover);

docs_row1.addComponent(resume_label);
docs_row1.addComponent(resume);

docs_row1.addComponent(profilbild_label);
docs_row1.addComponent(profilbild);

docs_row1.addComponent(more_label);
docs_row1.addComponent(more);

docs_row1.setMargin(true);
docs_row1.setSpacing(true);


Label lebenslauf_bisher = new Label("Bisher gespeicherter Lebenslauf:");
Link lebenslauf_path = new Link("20160321_Lebenslauf_SM_aktuell.pdf", new ExternalResource("www.google.com"));

Label cover_bisher = new Label("Bisher gespeichertes Anschreiben:");
Link cover_path = new Link("anschreiben_SM.pdf", new ExternalResource("www.google.com"));


Label resume_bisher = new Label("Bisher gespeicherte Zeugnisse:");
Link resume_path = new Link("Zeugnisse_vollstaendig.pdf", new ExternalResource("www.google.com"));

Label profbild_bisher = new Label("Bisher gespeichertes Profilbild:");
Link profbild_path = new Link("Sebastian.png", new ExternalResource("www.google.com"));

Label weitere_bisher = new Label("Weitere Anhänge:");
Link weitere_path = new Link("gesammelte_werke.pdf", new ExternalResource("www.google.com"));




docs_row2.addComponent(lebenslauf_bisher);
docs_row2.addComponent(lebenslauf_path);
docs_row2.addComponent(cover_bisher);
docs_row2.addComponent(cover_path);
docs_row2.addComponent(resume_bisher);
docs_row2.addComponent(resume_path);
docs_row2.addComponent(profbild_bisher);
docs_row2.addComponent(profbild_path);
docs_row2.addComponent(weitere_bisher);
docs_row2.addComponent(weitere_path);




docs_row2.setMargin(true);
docs_row2.setSpacing(true);

docs.addComponent(docs_row1);
// nicht nötig, da ersteingabe.
docs.addComponent(docs_row2);

panel_docs.setContent(docs);
panel_docs.setSizeFull();

Label aktionenBisher = new Label("Bisherige Aktionen:");
HorizontalLayout eingabeAktionen = new HorizontalLayout();
final DateField datumAktionen = new DateField("Datum:");
final ComboBox aktionAuswahl = new ComboBox("Aktion:");

aktionAuswahl.addItem("Vorgestellt bei Kunde");
aktionAuswahl.addItem("Akzeptiert von Kunde");
aktionAuswahl.addItem("Abgelehnt von Kunde");
aktionAuswahl.addItem("Vorstellungsgespräch");
aktionAuswahl.addItem("Telefonat");


datumAktionen.setWidth(100, Sizeable.Unit.PERCENTAGE);
aktionAuswahl.setWidth(100, Sizeable.Unit.PERCENTAGE);


final ComboBox aktionKunde = new ComboBox("Kunde:");

aktionKunde.addItem("Lanxess Leverkusen");
aktionKunde.addItem("Netorium GmbH");

aktionKunde.setWidth(100, Sizeable.Unit.PERCENTAGE);
final TextField aktionBemerkung = new TextField("Kommentar:");
aktionBemerkung.setWidth(140, Sizeable.Unit.PERCENTAGE);

Button hinzu = new Button("Hinzufügen");

eingabeAktionen.addComponent(datumAktionen);
eingabeAktionen.addComponent(aktionAuswahl);
eingabeAktionen.addComponent(aktionKunde);
eingabeAktionen.addComponent(aktionBemerkung);

eingabeAktionen.addComponent(hinzu);
eingabeAktionen.setComponentAlignment(hinzu, Alignment.BOTTOM_RIGHT);
eingabeAktionen.setWidth(80, Sizeable.Unit.PERCENTAGE);

Label aktionen_header = new Label("Aktionen:");
aktionen.addComponent(aktionen_header);
aktionen.addComponent(eingabeAktionen);

  final Grid aktionen_table = new Grid();
    aktionen_table.setContainerDataSource(aktionen_bean);
   aktionen_bean.setBeanIdProperty("datum");
   aktionen_table.setColumnOrder("datum","aktion","kunde","kommentar");
  //  aktionen_bean.addBean(new GD2_BewerberAktionen_datasource("14.03.2016", "Vorgestellt bei Kunde", "Netorium GmbH", "lief gut."));
  //  aktionen_bean.addBean(new GD2_BewerberAktionen_datasource("22.03.2016", "Abgelehnt von Kunde", "Netorium GmbH", "lief nicht so gut."));
    aktionen_table.setEditorEnabled(false); 
    aktionen_table.setSizeUndefined();
    aktionen_table.setHeightUndefined();
   aktionen_table.setHeight(30, Sizeable.Unit.PERCENTAGE);
   aktionen_table.setWidth(100, Sizeable.Unit.PERCENTAGE);
aktionen.setMargin(true);
aktionen.setSpacing(true);
aktionen.addComponent(aktionen_table);

panel_aktionen.setContent(aktionen);
panel_aktionen.setSizeFull();


hinzu.addClickListener(new Button.ClickListener(){
    
    public void buttonClick(Button.ClickEvent event){
        
       DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
 
    
  aktionen_bean.addBean(new GD2_BewerberAktionen_datasource(dateFormat.format(datumAktionen.getValue()), aktionAuswahl.getItemCaption(aktionAuswahl.getValue()), aktionKunde.getItemCaption(aktionKunde.getValue()), aktionBemerkung.getValue()));
    
    }

      
    });

TabSheet unterlagen = new TabSheet();
unterlagen.setSizeFull();
unterlagen.addTab(bericht_layout, "Kurzprofil erstellen");
unterlagen.addTab(vertrag_layout, "Vertragsunterlagen erstellen");
Button bericht = new Button("Kurzprofil erstellen");
Button vollbild = new Button("Vollbild");
Button reset = new Button("Reset");




create_expose.addComponent(unterlagen);

create_expose.setMargin(true);
create_expose.setSpacing(true);

panel_expose.setContent(create_expose);
panel_expose.setSizeFull();


final CheckBox klarname = new CheckBox("Vollen Namen verwenden");
final CheckBox lelauf = new CheckBox("Lebenslauf vollständig anhängen");
final CheckBox profbild = new CheckBox("Profilbild verwenden");
final RichTextArea richText = new RichTextArea();
richText.setWidth(210, Sizeable.Unit.MM);
richText.setHeight(297, Sizeable.Unit.MM);



richText.setValue("<html>" +
                  "<head>" +
		"<title></title>" + 
                    "</head>" + 
                    "<body>" + 
	"	<h3 style='color:blue;'>" +
	"<span style=\"color:#000000;\"><span style=\"font-size:28px;\"><u><strong>Bericht</strong></u><strong> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong><u><strong><img alt=\"\" src=\'./images/grimm.png' style=\"width: 111px; height: 67px;\" /></strong></u></span></span></h3>" +
	"	<h3 style='color:blue;'>" + 
	"		&nbsp;</h3>"      +
	"	<h3 style='color:blue;'>" +
	"		<span style='color:#000000;'><span style='font-size: 18px;'><u>Mitarbeiter:</u> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Sebastian Manemann</span></span></h3>" + 
	"	<h3 style='color:blue;'>" + 
	"		<span style='color:#000000;'><u><span style='font-size: 18px;'>Erlernter Beruf:</span></u><span style='font-size: 18px;'> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Produktmanager</span></span></h3>" + 
	"	<h3 style='color: blue;'>" + 
	"		<span style='color:#000000;'><u><span style='font-size: 18px;'>Wohnhaft:</span></u><span style='font-size: 18px;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;H&ouml;henstra&szlig;e 36a, 51491 Overath</span></span></h3>" + 
	"	<p>" +
	"		&nbsp;</p>" + 
	"	<h3 style='color:blue;'>" + 
	"		<span style='color:#000000;'><u><span style='font-size:22px;'>Beruflicher Werdegang:</span></u></span></h3>" + 
	"	<h3 style='color:blue;'>" + 
	"		&nbsp;</h3>" +
	"	<h3 style='color:blue;'>" +
	"		<span style='color:#000000;'><span style='font-size: 18px;'>&nbsp; 2006 - 2015 : &nbsp; &nbsp; &nbsp; &nbsp; Koordinator Transcoding, WDR mediagroup GmbH, K&ouml;ln&nbsp;</span></span></h3>" + 
	"</body>" + 
"</html>" 
);

bericht_row1.addComponent(klarname);
bericht_row1.addComponent(lelauf);
bericht_row1.addComponent(profbild);
bericht_row1.setSpacing(true);

bericht_layout.addComponent(bericht_row1);
bericht_layout.addComponent(richText);
bericht_row2.addComponent(bericht);
bericht_row2.addComponent(vollbild);
bericht_row2.addComponent(reset);
bericht_row2.setSpacing(true);
bericht_layout.addComponent(bericht_row2);
bericht_layout.setMargin(true);
bericht_layout.setSpacing(true);
bericht_layout.setSizeFull();
bericht_layout.setExpandRatio(bericht_row1, 0.1f);
bericht_layout.setExpandRatio(richText, 0.8f);
bericht_layout.setExpandRatio(bericht_row2, 0.1f);



//mainLayout.addComponent(memo);
//mainLayout.addComponent(save2);

notizen.addComponent(memo);
notizen.addComponent(save2);

save2.addClickListener(new Button.ClickListener(){
   public void buttonClick(Button.ClickEvent event){
       DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd  HH:mm");
       Date date = new Date();
       System.out.println(dateFormat.format(date));

       
       notizen.addComponent(new Label(dateFormat.format(date)+" - "+VaadinSession.getCurrent().getAttribute("User").toString()+" - "+ memo.getValue()));
      
       memo.setValue("");
    }

      
    });

notizen.setMargin(true);
notizen.setSpacing(true);

panel_notizen.setContent(notizen);
panel_notizen.setSizeFull();




return add_window;
}    
  
    
}
