/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.bewerber;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author temp
 */
public class GD2_BewerberLebenslauf_datasource implements Serializable{
    
    
String vonDatum;
 String bisDatum;
 String taetigkeit;
 String details;
 String firma;
 Boolean zeitArbeit;
 String zeitArbeitFirma;
    
   
    
 

public GD2_BewerberLebenslauf_datasource(String vonDatum, String bisDatum, String taetigkeit, String details, String firma,Boolean zeitArbeit, String zeitArbeitFirma) {
this.vonDatum = vonDatum;
this.bisDatum = bisDatum;
this.taetigkeit = taetigkeit;
this.details = details;
this.firma = firma;
this.zeitArbeit = zeitArbeit;
this.zeitArbeitFirma = zeitArbeitFirma;
}
  
public String getVonJahr () {

return vonDatum;
}


public void setVonJahr(String vonDatum) {

this.vonDatum = vonDatum;
}
 
    
public String getBisJahr () {

return bisDatum;
}


public void setBisJahr(String bisDatum) {

this.bisDatum = bisDatum;
}
public String getTaetigkeit () {

return taetigkeit;
}


public void setTaetigkeit(String taetigkeit) {

this.taetigkeit = taetigkeit;
}

public String getDetails () {

return details;
}


public void setDetails(String details) {

this.details = details;
}

public String getFirma () {

return firma;
}


public void setFirma(String firma) {

this.firma = firma;
}

public Boolean getZeitarbeit () {

return zeitArbeit;
}


public void setZeitarbeit(Boolean zeitArbeit) {

this.zeitArbeit = zeitArbeit;
}

public String getZeitarbeitsfirma () {

return zeitArbeitFirma;
}


public void setZeitarbeitsfirma(String zeitArbeitFirma) {

this.zeitArbeitFirma = zeitArbeitFirma;
}



}

