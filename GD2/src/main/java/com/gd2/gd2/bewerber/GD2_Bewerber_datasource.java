/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.bewerber;

import java.io.Serializable;

/**
 *
 * @author temp
 */
public class GD2_Bewerber_datasource implements Serializable{
    
    
    Integer BewerberID;
    String vorname;
    String nachname;
    String kurzname;
    String adresse;
    String PLZ;
    String ort;
    String land;
    String telefon;
    String mobilnr;
    String email;
    String geburtsdatum;
    String nationalitaet;
    String beruf;
    String verantwortlStandort;
    String erfasstvonStandort;
    String erfasstvonPerson;
    String erfasstDatum;  
    
   
    
 

public GD2_Bewerber_datasource(Integer BewerberID,String vorname, String nachname, String kurzname, String adresse, String PLZ, String ort, String land,
        String telefon, String mobilnr, String email, String geburtsdatum, String nationalitaet,String beruf, String verantwortlStandort, 
        String erfasstvonStandort, String erfasstvonPerson, String erfasstDatum) {
this.BewerberID = BewerberID;    
this.vorname = vorname;
this.nachname = nachname;
this.kurzname = kurzname;
this.adresse = adresse;
this.PLZ = PLZ;
this.ort = ort;
this.land = land;
this.telefon = telefon;
this.mobilnr = mobilnr;
this.email = email;
this.geburtsdatum = geburtsdatum;
this.nationalitaet = nationalitaet;
this.beruf = beruf;
this.verantwortlStandort = verantwortlStandort;
this.erfasstvonStandort = erfasstvonStandort;
this.erfasstvonPerson = erfasstvonPerson;
this.erfasstDatum = erfasstDatum;


}

    GD2_Bewerber_datasource(double d) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
public Integer getBewerberID () {

return BewerberID;
}


public void setBewerberID(Integer BewID) {

this.BewerberID = BewID;
}
 
    
public String getVorname () {

return vorname;
}


public void setVorname(String vorname) {

this.vorname = vorname;
}
public String getNachname () {

return nachname;
}


public void setNachname(String nachname) {

this.nachname = nachname;
}
public String getKurzname () {

return kurzname;
}


public void setKurzname(String kurzname) {

this.kurzname = kurzname;
}
public String getAdresse () {

return adresse;
}


public void setAdresse(String adresse) {

this.adresse = adresse;
}
public String getPLZ () {

return PLZ;
}


public void setPLZ(String PLZ) {

this.PLZ = PLZ;
}
public String getOrt () {

return ort;
}


public void setOrt(String ort) {

this.ort = ort;
}
public String getLand () {

return land;
}


public void setLand(String land) {

this.land = land;
}
public String getTelefon () {

return telefon;
}


public void setTelefon(String telefon) {

this.telefon = telefon;
}
public String getMobilnr () {

return mobilnr;
}


public void setMobilnr(String mobilnr) {

this.mobilnr = mobilnr;
}
public String getEmail () {

return email;
}


public void setEmail(String email) {

this.email = email;
}
public String getGeburtsdatum () {

return geburtsdatum;
}


public void setGeburtsdatum(String geburtsdatum) {

this.geburtsdatum = geburtsdatum;
}
public String getNationalitaet () {

return nationalitaet;
}


public void setNationalitaet(String nationalitaet) {

this.nationalitaet = nationalitaet;
}
public String getBeruf () {

return beruf;
}


public void setBeruf(String beruf) {

this.beruf = beruf;
}
public String getVerantwStandort () {

return verantwortlStandort;
}


public void setVerantwStandort(String verantwortlStandort) {

this.verantwortlStandort = verantwortlStandort;
}
public String getErfasstVonStandort () {

return erfasstvonStandort;
}


public void setErfasstVonStandort(String erfasstvonStandort) {

this.erfasstvonStandort = erfasstvonStandort;
}
public String getErfasstVonPerson () {

return erfasstvonPerson;
}


public void setErfasstVonPerson(String erfasstvonPerson) {

this.erfasstvonPerson = erfasstvonPerson;
}
public String getErfasstDatum() {

return erfasstDatum;
}


public void setErfasstDatum(String erfasstDatum) {

this.erfasstDatum = erfasstDatum;
}




}

