/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.bewerber;


import static com.gd2.gd2.bewerber.GD2_Bewerber_edit.lebenslauf_bean;
import com.gd2.gd2.user.GD2_Users_Datasource;
import com.vaadin.client.renderers.ButtonRenderer;
//import com.vaadin.client.renderers.HtmlRenderer;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.naming.NamingException;

/**
 *
 * @author temp
 */


public class GD2_Bewerber_add {
    
    
    public Window add_window = new Window();
    VerticalLayout mainLayout = new VerticalLayout();
    TabSheet tabs = new TabSheet();
    
   public HorizontalLayout persDaten = new HorizontalLayout();
   public HorizontalLayout buttons = new HorizontalLayout();

   
   public HorizontalLayout berufDaten = new HorizontalLayout();
   public VerticalLayout zust = new VerticalLayout();
   public VerticalLayout lebenslauf_layout = new VerticalLayout();
   public VerticalLayout zusatzDaten = new VerticalLayout();
   public HorizontalLayout docs = new HorizontalLayout();
   public VerticalLayout docs_row1 = new VerticalLayout();
   public VerticalLayout docs_row2 = new VerticalLayout();

   public Button save = new Button ("Speichern");
   public Button exit = new Button ("schliessen");
   public Button hinzu = new Button("Hinzufügen");
   public Button hinzu_sprache = new Button("Hinzufügen");
   public Button hinzu_kenntn = new Button("Hinzufügen");
   
   
   public Panel panel_docs = new Panel();
   public Panel panel_persDaten = new Panel();
   public Panel panel_berufDaten = new Panel();
   public Panel panel_zust = new Panel();
   
   
    
     public static BeanContainer<String, GD2_BewerberLebenslauf_datasource> lebenslauf_bean = new BeanContainer<>(GD2_BewerberLebenslauf_datasource.class);
public Window getWindow() throws SQLException, NamingException {

    
  save.setStyleName("friendly");
  exit.setStyleName("primary");
  tabs.addTab(persDaten,"Persönliche Daten");
  tabs.addTab(berufDaten,"Berufliche Daten");
  tabs.addTab(lebenslauf_layout, "Lebenslauf");
  tabs.addTab(zust,"Zuständigkeit");

    
  Label gesamtdaten = new Label("Gesamtdatensatz:");
  
mainLayout.addComponent(tabs);
tabs.addStyleName(ValoTheme.TABSHEET_CENTERED_TABS);
tabs.setSizeFull();
//mainLayout.addComponent(new Label("<hr />",Label.CONTENT_XHTML));
mainLayout.setMargin(true);
mainLayout.setSpacing(true);

        add_window.setWidth(70, Sizeable.Unit.PERCENTAGE);
        add_window.setHeight(80, Sizeable.Unit.PERCENTAGE);
        add_window.center();
    
        UI ui = UI.getCurrent();
        add_window.setContent(mainLayout);
        ui.addWindow(add_window);
        
buttons.addComponent(gesamtdaten);
buttons.addComponent(save);
buttons.addComponent(exit);
buttons.setMargin(true);
buttons.setSpacing(true);

mainLayout.addComponent(buttons);
mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_LEFT);
mainLayout.setExpandRatio(buttons, 0.1f);
mainLayout.setExpandRatio(tabs, 0.9f);
mainLayout.setSizeFull();




VerticalLayout pic = new VerticalLayout();
        pic.setSizeUndefined();
        pic.setSpacing(true);
        Image profilePic = new Image(null, new ThemeResource(
                "images/dummy-profile-pic-male.jpg"));
        profilePic.setWidth(100.0f, Unit.PIXELS);
        pic.addComponent(profilePic);

        Button upload = new Button("Ändern…", new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                Notification.show("Not implemented in this demo");
            }
        });
        upload.addStyleName(ValoTheme.BUTTON_TINY);
        upload.addStyleName(ValoTheme.BUTTON_PRIMARY);
        pic.addComponent(upload);


ComboBox titel = new ComboBox("Anrede");
titel.addItem("Herr");
titel.addItem("Frau");
titel.setRequired(true);

ComboBox grad = new ComboBox("Akad. Grad");
grad.addItem("Dr.");
grad.addItem("Prof.");
grad.addItem("Dipl. Ing.");
grad.addItem("B.A.");
grad.addItem("B.Sc.");
grad.addItem("B.Eng.");
grad.addItem("M.A.");



TextField vorName = new TextField("Vorname");
vorName.setRequired(true);
TextField nachName = new TextField("Nachname");
nachName.setRequired(true);
TextField kurzName = new TextField("Kurzname");
kurzName.setRequired(true);

TextField straße = new TextField("Straße");

TextField nr = new TextField("Nr");

TextField zusatz = new TextField("Zusatz");




TextField PLZ = new TextField("PLZ");

TextField Ort = new TextField("Ort");

TextField birthday = new TextField("Geburtstag");


ComboBox nat = new ComboBox("Nationalität");

nat.addItems("Belgien","Deutschland","Dänemark","Finnland","Frankreich","Griechenland","Großbritanien","Irland","Italien","Luxemburg","Niederlande","Österreich","Portugal","Schweden","Spanien","Estland","Lettland","Litauen","Polen",
               "Slowakische Rep.","Slowenien","Tschech. Rep.","Ungarn","Malta","Zypern","Island","Lichtenstein","Norwegen","Schweiz","Bulgarien","Rumänien");


//Blacklist countries



List<String> blacklist = new ArrayList<String>();
blacklist.add("Türkei");
blacklist.add("Syrien");
blacklist.add("Iran");
blacklist.add("Irak");
blacklist.add("Pakistan");
blacklist.add("Russland");
blacklist.add("andere");

nat.addItems(blacklist);
nat.setNullSelectionAllowed(false);
Button nat_hinzu = new Button("Nationalität nicht vorhanden");
nat_hinzu.addStyleName(ValoTheme.BUTTON_PRIMARY);
nat_hinzu.addStyleName(ValoTheme.BUTTON_SMALL);

TextField nationalitaet = new TextField("Nationalität");
nationalitaet.setVisible(false);

CheckBox nat_blacklist = new CheckBox("Nicht EU");
nat_blacklist.setVisible(false);
nat_blacklist.addStyleName(ValoTheme.CHECKBOX_SMALL);

Button add_nat = new Button("Hinzufügen");
add_nat.addStyleName(ValoTheme.BUTTON_PRIMARY);
add_nat.addStyleName(ValoTheme.BUTTON_SMALL);
add_nat.setVisible(false);


TextField email = new TextField("Email Adresse");

TextField phone = new TextField ("Telefonnummer");

TextField mobile = new TextField("Mobilnummer");

TextField website = new TextField ("Webseite");

TextArea comment = new TextArea("Kommentare");
comment.setWidth("100%");
comment.setRows(4);


CheckBox fuehers = new CheckBox("Führerschein");
fuehers.addStyleName(ValoTheme.CHECKBOX_SMALL);

CheckBox fahrz = new CheckBox("Fahrzeug");
fahrz.addStyleName(ValoTheme.CHECKBOX_SMALL);


TwinColSelect fSchein = new TwinColSelect("Führerschein");
fSchein.addItem("keine");
fSchein.addItem("Klasse A1");
fSchein.addItem("Klasse A");
fSchein.addItem("Klasse B");
fSchein.addItem("Klasse C1");
fSchein.addItem("Klasse C");
fSchein.addItem("Klasse D1");
fSchein.addItem("Klasse D");
fSchein.addItem("Klasse BE");
fSchein.addItem("Klasse C1E");
fSchein.addItem("Klasse CE");
fSchein.addItem("Klasse D1E");
fSchein.addItem("Klasse DE");
fSchein.addItem("Klasse M");
fSchein.addItem("Klasse L");
fSchein.setVisible(false);

ComboBox fahrzeug = new ComboBox("Fahrzeug");
fahrzeug.addItem("PKW");
fahrzeug.addItem("Motorrad");
fahrzeug.addItem("Motorroller");
fahrzeug.setVisible(false);
fahrzeug.setNullSelectionAllowed(false);

TextField kennzeichen = new TextField("Amtl. Kennzeichen");
kennzeichen.setVisible(false);


OptionGroup arbeitserlaubnis = new OptionGroup("Arbeitserlaubnis oder Arbeitsgenehmigung");
arbeitserlaubnis.addItem("vorhanden");
arbeitserlaubnis.addItem("nicht vorhanden");
arbeitserlaubnis.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
arbeitserlaubnis.setVisible(false);
arbeitserlaubnis.addStyleName(ValoTheme.LABEL_FAILURE);

DateField ab_gueltig_bis = new DateField("Arbeitserlaubnis gültig bis");
ab_gueltig_bis.setVisible(false);
ab_gueltig_bis.addStyleName(ValoTheme.LABEL_FAILURE);

DateField auf_gueltig_bis = new DateField("Aufenthaltsgenehmigung gültig bis");
auf_gueltig_bis.setVisible(false);
auf_gueltig_bis.addStyleName(ValoTheme.LABEL_FAILURE);

TextField seit_wann = new TextField("Seit wann leben Sie in Deutschland");
seit_wann.setVisible(false);
seit_wann.addStyleName(ValoTheme.LABEL_FAILURE);



Label persoenlich = new Label("Persönliche Daten");

        persoenlich.addStyleName(ValoTheme.LABEL_H4);
        persoenlich.addStyleName(ValoTheme.LABEL_COLORED);
        
        
Label anschrift = new Label("Anschrift");

        anschrift.addStyleName(ValoTheme.LABEL_H4);
        anschrift.addStyleName(ValoTheme.LABEL_COLORED);
        
Label kontakt = new Label("Kontakt");

        kontakt.addStyleName(ValoTheme.LABEL_H4);
        kontakt.addStyleName(ValoTheme.LABEL_COLORED);
      
                
Label mobil = new Label("Mobilität");

        mobil.addStyleName(ValoTheme.LABEL_H4);
        mobil.addStyleName(ValoTheme.LABEL_COLORED);      
        
        
Label achtung = new Label("Achtung, diese Nationalität erfordert zusätzliche Angaben!");

        achtung.addStyleName(ValoTheme.LABEL_H4);
        achtung.addStyleName(ValoTheme.LABEL_COLORED);        
        achtung.setVisible(false);
        
Label nat_label = new Label("Nationalität Hinzufügen");

        nat_label.addStyleName(ValoTheme.LABEL_H4);
        nat_label.addStyleName(ValoTheme.LABEL_COLORED);        
        nat_label.setVisible(false);

       
       
       
        
        


        FormLayout details = new FormLayout();
        details.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
        details.setMargin(true);
        details.setSpacing(true);
        

        persDaten.addStyleName("profile-form");
        persDaten.addComponent(pic);
        persDaten.addComponent(details);
        persDaten.setExpandRatio(details, 1);
        persDaten.setComponentAlignment(details, Alignment.TOP_RIGHT);
        persDaten.setMargin(true);
        persDaten.setSpacing(true);
        persDaten.setWidth(100.0f, Unit.PERCENTAGE);
        
        details.addComponent(persoenlich);
        details.addComponent(titel);
        details.addComponent(grad);
        details.addComponent(vorName);
        details.addComponent(nachName);
        details.addComponent(kurzName);
        details.addComponent(birthday);
        details.addComponent(nat);
        details.addComponent(nat_hinzu);
        details.addComponent(nat_label);
        details.addComponents(nationalitaet,nat_blacklist,add_nat);
        
        details.addComponent(achtung);
        details.addComponents(arbeitserlaubnis,ab_gueltig_bis,auf_gueltig_bis,seit_wann);
        
        
        
        details.addComponent(anschrift);
        details.addComponents(straße,nr,zusatz, PLZ, Ort);
        
        details.addComponent(kontakt);
        details.addComponents(email,phone,mobile,website,comment);
        
        
        details.addComponent(mobil);
        details.addComponents(fuehers,fSchein,fahrz,fahrzeug,kennzeichen);
        
        
        nat.addValueChangeListener(new ValueChangeListener() {

      @Override
      public void valueChange(Property.ValueChangeEvent event) {
          
              if (blacklist.contains(nat.getValue()))   {
               achtung.setVisible(true);
               arbeitserlaubnis.setVisible(true);
               ab_gueltig_bis.setVisible(true);
               auf_gueltig_bis.setVisible(true);
               seit_wann.setVisible(true);
           }
           
              else if (!blacklist.contains(nat.getValue()) && achtung.isVisible() == true)   {
               achtung.setVisible(false);
               arbeitserlaubnis.setVisible(false);
               ab_gueltig_bis.setVisible(false);
               auf_gueltig_bis.setVisible(false);
               seit_wann.setVisible(false);
               
           }
       
      }
  });
        
        add_nat.addClickListener(new Button.ClickListener() {

      @Override
      public void buttonClick(ClickEvent event) {
         if(nat_blacklist.getValue() == true && nationalitaet.getValue() != ""){
             nat.addItem(nationalitaet.getValue());
             blacklist.add(nationalitaet.getValue());
             nationalitaet.setVisible(false);
             nationalitaet.setValue("");
             nat_blacklist.setVisible(false);
             add_nat.setVisible(false);
             
             
         }
         
         else if(nat_blacklist.getValue() == false && nationalitaet.getValue() != ""){
         
             nat.addItem(nationalitaet.getValue());
               nationalitaet.setVisible(false);
               nationalitaet.setValue("");
               nat_blacklist.setVisible(false);
               add_nat.setVisible(false);
         }
         
         else if(nationalitaet.getValue() == ""){
         Notification.show("Bitte machen Sie eine Eingabe");
         }
      }
      
  });
        
        
     
       
       fuehers.addValueChangeListener((Property.ValueChangeEvent event) -> {
           if(fuehers.getValue().equals(true)){
               
               fSchein.setVisible(true);
               
           }
           
           if(!fuehers.getValue().equals(true)){
               
               fSchein.setVisible(false);
               
           }
  });
       
       fahrz.addValueChangeListener((Property.ValueChangeEvent event) -> {
           if(fahrz.getValue().equals(true)){
               fahrzeug.setVisible(true);
               
           }
           
           if(!fahrz.getValue().equals(true)){
               fahrzeug.setVisible(false);
               kennzeichen.setVisible(false);
              
               
           }
  });
       
       fahrzeug.addValueChangeListener((Property.ValueChangeEvent event) -> {
           if(fahrzeug.getValue().equals("PKW") || fahrzeug.getValue().equals("Motorrad")||fahrzeug.getValue().equals("Motorroller")){
               
               kennzeichen.setVisible(true);
               
           }
  });
       
       nat_hinzu.addClickListener(new Button.ClickListener() {

      @Override
      public void buttonClick(ClickEvent event) {
         if(nationalitaet.isVisible() == true){
         nationalitaet.setVisible(false);
         nationalitaet.setValue("");
         nat_blacklist.setVisible(false);
         add_nat.setVisible(false);
         }
         
         else if(nationalitaet.isVisible() == false){
         nationalitaet.setVisible(true);
        
         nat_blacklist.setVisible(true);
         add_nat.setVisible(true);
         }
         
      }
  });
       
       

ComboBox beruf = new ComboBox("abgeschl. Berufsausbildung");
ComboBox beruf2 = new ComboBox("weitere Berufsausbildung");
beruf2.setVisible(false);

ComboBox beruf3 = new ComboBox("weitere Berufsausbildung");
beruf3.setVisible(false);


ComboBox beruf4 = new ComboBox("weitere Berufsausbildung");
beruf4.setVisible(false);

        GD2_Users_Datasource jobs = new GD2_Users_Datasource();
        jobs.Exec_query_jobs();
        
        for (int i = 0; i < jobs.jobs.size(); i++) {
        beruf.addItem(jobs.jobs.get(i));
        beruf2.addItem(jobs.jobs.get(i));
        beruf3.addItem(jobs.jobs.get(i));
        beruf4.addItem(jobs.jobs.get(i));}



//
//HorizontalLayout lebenslauf_layout = new HorizontalLayout();
final TextField datumVon = new TextField("Von Jahr");

final TextField datumBis = new TextField("Bis Jahr");

final ComboBox Taetigkeit = new ComboBox("Tätigkeit");


      GD2_Users_Datasource doings = new GD2_Users_Datasource();
        doings.Exec_query_doings();
        
        for (int i = 0; i < doings.doings.size(); i++) {
            if (doings.doings.get(i)!= null){
            Taetigkeit.addItem(doings.doings.get(i));}}

final TextField detail_taet = new TextField("Details");

final TextField Firma = new TextField("Firma");



final CheckBox zeitArbeit = new CheckBox("Zeitarbeit?");
zeitArbeit.addStyleName(ValoTheme.CHECKBOX_SMALL);



final ComboBox Arbeitgeber = new ComboBox("Zeitarbeitsfirma");
Arbeitgeber.setVisible(false);


TextField sprachen = new TextField("Sprachen");

ComboBox sprachniveau = new ComboBox("Sprachniveau");
sprachniveau.addItem("Muttersprache");
sprachniveau.addItem("Fliessend");
sprachniveau.addItem("Verständlich");
sprachniveau.addItem("Vokabeln");

Label sprache1 = new Label();
sprache1.setVisible(false);

Label sprache2 = new Label();
sprache2.setVisible(false);

Label sprache3 = new Label();
sprache3.setVisible(false);

Label sprache4 = new Label();
sprache4.setVisible(false);
Label sprache5 = new Label();
sprache5.setVisible(false);

Label sprache6 = new Label();
sprache6.setVisible(false);


TextArea kenntn = new TextArea("Kentnisse");
kenntn.setWidth("100%");
kenntn.setRows(4);
kenntn.setEnabled(false);
kenntn.setVisible(false);

TextField zusaetzlKentnisse = new TextField("Zusätzliche Kentnisse");


   final Grid lebenslauf_table = new Grid();
    lebenslauf_table.setContainerDataSource(lebenslauf_bean);
   lebenslauf_bean.setBeanIdProperty("vonJahr");
   lebenslauf_table.setColumnOrder("vonJahr","bisJahr","taetigkeit");
    lebenslauf_bean.addBean(new GD2_BewerberLebenslauf_datasource("2006", "2014", "Koordinator Transcoding","Erstellung Transcoding Workflows, Team Management", "WDR mediagroup Digital GmbH", false,""));
    lebenslauf_bean.addBean(new GD2_BewerberLebenslauf_datasource("2015", "2016", " Produktmanager","Produktmanagement Aspera & Broadpeak","Netorium GmbH",true,""));
    lebenslauf_table.setEditorEnabled(true); 
    lebenslauf_table.setSizeFull();
    lebenslauf_table.addStyleName(ValoTheme.TABLE_NO_STRIPES);
    
    
Grid.Column truth = lebenslauf_table.getColumn("zeitarbeit");
truth.setConverter(new StringToBooleanConverter(
        FontAwesome.CHECK_CIRCLE_O.getHtml(),
        FontAwesome.CIRCLE_O.getHtml()));

truth.setRenderer(new HtmlRenderer());



    

  
   
   
   hinzu.addClickListener(new Button.ClickListener(){
    
    public void buttonClick(Button.ClickEvent event){
    
  lebenslauf_bean.addBean(new GD2_BewerberLebenslauf_datasource(datumVon.getValue(), datumBis.getValue(), Taetigkeit.getValue().toString(),detail_taet.getValue(),Firma.getValue(),zeitArbeit.getValue(),Arbeitgeber.getId()));
    
    }

      
    });
   
   
   Label lebenslauf_label = new Label("Lebenslauf");

        lebenslauf_label.addStyleName(ValoTheme.LABEL_H4);
        lebenslauf_label.addStyleName(ValoTheme.LABEL_COLORED); 
   
   lebenslauf_layout.addComponent(lebenslauf_label);
   lebenslauf_layout.addComponent(lebenslauf_table);
   lebenslauf_layout.setExpandRatio(lebenslauf_table, 1);
   lebenslauf_layout.setSizeFull();
   lebenslauf_layout.setMargin(true);
   lebenslauf_layout.setSpacing(true);
   


hinzu.addStyleName(ValoTheme.BUTTON_TINY);
hinzu.addStyleName(ValoTheme.BUTTON_PRIMARY);

hinzu_sprache.addStyleName(ValoTheme.BUTTON_TINY);
hinzu_sprache.addStyleName(ValoTheme.BUTTON_PRIMARY);

hinzu_kenntn.addStyleName(ValoTheme.BUTTON_TINY);
hinzu_kenntn.addStyleName(ValoTheme.BUTTON_PRIMARY);


Label beruferl = new Label("Berufsausbildung");

        beruferl.addStyleName(ValoTheme.LABEL_H4);
        beruferl.addStyleName(ValoTheme.LABEL_COLORED);   
        
        
Label zusatzKentn = new Label("Zusätzliche Kentnisse");

        zusatzKentn.addStyleName(ValoTheme.LABEL_H4);
        zusatzKentn.addStyleName(ValoTheme.LABEL_COLORED);  
        
Label lebenslauf_hinzu = new Label("dem Lebenslauf Hinzufügen");

        lebenslauf_hinzu.addStyleName(ValoTheme.LABEL_H4);
        lebenslauf_hinzu.addStyleName(ValoTheme.LABEL_COLORED);     
        
Label lebenslauf = new Label("Lebenslauf");

        lebenslauf.addStyleName(ValoTheme.LABEL_H4);
        lebenslauf.addStyleName(ValoTheme.LABEL_COLORED);  
        
        
Label sprachen_lab = new Label("Sprachen");

        sprachen_lab.addStyleName(ValoTheme.LABEL_H4);
        sprachen_lab.addStyleName(ValoTheme.LABEL_COLORED);  




FormLayout details2 = new FormLayout();
        details2.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
        details2.setMargin(true);
        details2.setSpacing(true);
        

        //berufDaten.addStyleName("profile-form");
        //berufDaten.addComponent(pic);
        berufDaten.addComponent(details2);
        berufDaten.setExpandRatio(details2, 1);
        berufDaten.setComponentAlignment(details2, Alignment.TOP_RIGHT);
        berufDaten.setMargin(true);
        berufDaten.setSpacing(true);
        berufDaten.setWidth(100.0f, Unit.PERCENTAGE);

        details2.addComponent(beruferl);
        details2.addComponents(beruf,beruf2,beruf3,beruf4);
        
        details2.addComponent(zusatzKentn);
        details2.addComponents(zusaetzlKentnisse,hinzu_kenntn, kenntn);
        
        
        
        details2.addComponent(lebenslauf_hinzu);
        details2.addComponents(datumVon,datumBis,Taetigkeit,detail_taet,Firma,zeitArbeit,Arbeitgeber,hinzu);
        
        details2.addComponent(sprachen_lab);
        details2.addComponents(sprachen,sprachniveau,hinzu_sprache,sprache1,sprache2,sprache3,sprache4,sprache5,sprache6);
        
        
        
        
        
        
        beruf.addValueChangeListener(new ValueChangeListener() {

      @Override
      public void valueChange(Property.ValueChangeEvent event) { 
        if(beruf.isEmpty() == false)  {
        beruf2.setVisible(true);}
        
        if(beruf.isEmpty() == true){
        beruf2.setVisible(false);
        beruf3.setVisible(false);
        beruf4.setVisible(false);
        }
      
      }
 
        
      
  });
        beruf2.addValueChangeListener(new ValueChangeListener() {

      @Override
      public void valueChange(Property.ValueChangeEvent event) {
          if(beruf2.isEmpty() == false){  
        beruf3.setVisible(true);}
        if(beruf2.isEmpty() == true){
        beruf2.setVisible(false);
        beruf3.setVisible(false);
        beruf4.setVisible(false);
        }
        
      }
  });
        
        beruf3.addValueChangeListener(new ValueChangeListener() {

      @Override
      public void valueChange(Property.ValueChangeEvent event) {
        if(beruf3.isEmpty() == false){  
        beruf4.setVisible(true);}
        if(beruf3.isEmpty() == true){
        beruf3.setVisible(false);
        beruf4.setVisible(false);
        }
       
      }
      
      
  });
        beruf4.addValueChangeListener(new ValueChangeListener() {

      @Override
      public void valueChange(Property.ValueChangeEvent event) {

        if(beruf4.isEmpty() == true){
        beruf4.setVisible(false);
        }  
      }
  });


        
       zeitArbeit.addValueChangeListener(new ValueChangeListener() {

      @Override
      public void valueChange(Property.ValueChangeEvent event) {
          if(zeitArbeit.getValue().equals(true)){
          
           Arbeitgeber.setVisible(true);
          }
          
           if(zeitArbeit.getValue().equals(false)){
          
           Arbeitgeber.setVisible(false);
          }
          
      }
  });
       
       hinzu_sprache.addClickListener(new Button.ClickListener() {

      @Override
      public void buttonClick(ClickEvent event) {
          
          if(sprache1.isVisible() == false){
          sprache1.setVisible(true);
          sprache1.setValue(sprachen.getValue() + " - " + sprachniveau.getValue());
          sprachen.setValue("");
          sprachniveau.setValue("");
          }
          else if(sprache1.isVisible() == true && sprache2.isVisible() == false){
          sprache2.setVisible(true);
          sprache2.setValue(sprachen.getValue() + " - " + sprachniveau.getValue());
          sprachen.setValue("");
          sprachniveau.setValue("");
          }
          else if(sprache2.isVisible() == true && sprache3.isVisible() == false){
          sprache3.setVisible(true);
          sprache3.setValue(sprachen.getValue() + " - " + sprachniveau.getValue());
          sprachen.setValue("");
          sprachniveau.setValue("");
          }
          else if(sprache3.isVisible() == true && sprache4.isVisible() == false){
          sprache4.setVisible(true);
          sprache4.setValue(sprachen.getValue() + " - " + sprachniveau.getValue());
          sprachen.setValue("");
          sprachniveau.setValue("");
          }
          else if(sprache4.isVisible() == true && sprache5.isVisible() == false){
          sprache5.setVisible(true);
          sprache5.setValue(sprachen.getValue() + " - " + sprachniveau.getValue());
          sprachen.setValue("");
          sprachniveau.setValue("");
          }
          else if(sprache5.isVisible() == true && sprache6.isVisible() == false){
          sprache6.setVisible(true);
          sprache6.setValue(sprachen.getValue() + " - " + sprachniveau.getValue());
          sprachen.setValue("");
          sprachniveau.setValue("");
          }
          
          
      }
  });
       
       
        hinzu_kenntn.addClickListener(new Button.ClickListener() {

      @Override
      public void buttonClick(ClickEvent event) {
          if(kenntn.isVisible() == false && kenntn.getValue().equals("")){
              kenntn.setVisible(true);
          kenntn.setValue(zusaetzlKentnisse.getValue());
          zusaetzlKentnisse.setValue("");
          }
          
      
      
      else {
       
          kenntn.setValue(kenntn.getValue() + " ,"+ zusaetzlKentnisse.getValue());
          zusaetzlKentnisse.setValue("");
          
        }}
  });
        


        

Label Standort = new Label("Leverkusen - Hauptsitz");
Standort.setCaption("Erfasst von Niederlassung");


Label bewIDgen = new Label("2016011000");
bewIDgen.setCaption("Bewerber ID:");

ComboBox standort = new ComboBox("Niederlassung Zuweisen");
        GD2_Users_Datasource sites = new GD2_Users_Datasource();
        sites.Exec_query_sites();
        
        for (int i = 0; i < sites.sites.size(); i++) {
        standort.addItem(sites.sites.get(i));}



DateField eingangDBW = new DateField("Eingang der Bewerbung");
eingangDBW.setValue(new Date());

OptionGroup verfuegbar_zeitpunkt = new OptionGroup("Verfügbarkeit");
verfuegbar_zeitpunkt.addItem("Verfügbar Sofort");
verfuegbar_zeitpunkt.addItem("Verfügbar ab");
verfuegbar_zeitpunkt.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
verfuegbar_zeitpunkt.addStyleName(ValoTheme.OPTIONGROUP_SMALL);


DateField verfuegbar_ab = new DateField("Verfügbar ab");
verfuegbar_ab.setVisible(false);


ComboBox verfuegbar_radius = new ComboBox("Verfügbarkeitsradius");
verfuegbar_radius.addItem("10 km");
verfuegbar_radius.addItem("25 km");
verfuegbar_radius.addItem("50 km");
verfuegbar_radius.addItem("75 km");
verfuegbar_radius.addItem("100 km");
verfuegbar_radius.addItem(">100 km");

CheckBox umzug = new CheckBox("Umzug möglich");
umzug.addStyleName(ValoTheme.CHECKBOX_SMALL);


Upload lebenslauf_upload = new Upload();
lebenslauf_upload.setCaption("Original Lebenslauf anhängen");

Label lebenslauf_angehaengt = new Label("sebastian123.pdf");
lebenslauf_angehaengt.setVisible(false);

Upload cover = new Upload();
cover.setCaption("Anschreiben anhängen");

Upload resume = new Upload();
resume.setCaption("Zeugnisse anhängen");

Upload short_lebenslauf = new Upload();
short_lebenslauf.setCaption("Anhang zu Kurzbericht");

Upload more = new Upload();
more.setCaption("weitere Anhänge");


Label niederlassung = new Label("Niederlassung");

        niederlassung.addStyleName(ValoTheme.LABEL_H4);
        niederlassung.addStyleName(ValoTheme.LABEL_COLORED); 
        
Label verfuegbarkeit = new Label("Verfügbarkeit");

        verfuegbarkeit.addStyleName(ValoTheme.LABEL_H4);
        verfuegbarkeit.addStyleName(ValoTheme.LABEL_COLORED);         
        
Label dokumente = new Label("Dokumente");

        dokumente.addStyleName(ValoTheme.LABEL_H4);
        dokumente.addStyleName(ValoTheme.LABEL_COLORED);  

        


 FormLayout details3 = new FormLayout();
        details3.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
        details3.setMargin(true);
        details3.setSpacing(true);
        

        zust.addStyleName("profile-form");
        
        zust.addComponent(details3);
        zust.setExpandRatio(details3, 1);
        zust.setComponentAlignment(details3, Alignment.TOP_RIGHT);
        zust.setMargin(true);
        zust.setSpacing(true);
        zust.setWidth(100.0f, Unit.PERCENTAGE);
        
        details3.addComponent(niederlassung);
        details3.addComponents(Standort,standort,eingangDBW);
        
        details3.addComponent(verfuegbarkeit);
        details3.addComponents(verfuegbar_radius,umzug,verfuegbar_zeitpunkt,verfuegbar_ab);
        
        details3.addComponent(dokumente);
        details3.addComponents(lebenslauf_upload,cover,resume,short_lebenslauf,more);
       
        
        verfuegbar_zeitpunkt.addValueChangeListener(new ValueChangeListener() {

      @Override
      public void valueChange(Property.ValueChangeEvent event) {
          if(verfuegbar_zeitpunkt.getValue() == "Verfügbar Sofort"){
              
              verfuegbar_ab.setVisible(false);
          }
      
          else if(verfuegbar_zeitpunkt.getValue() == "Verfügbar ab") {
            verfuegbar_ab.setVisible(true);
        }}
      
  });


 





mainLayout.setSizeFull();




return add_window;
}    
}  
    

