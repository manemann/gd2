/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gd2.gd2.bewerber;

import static com.gd2.gd2.bewerber.GD2_Bewerber_add.lebenslauf_bean;
import static com.gd2.gd2.init.GD2_Bewerber.applicants;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.vaadin.tokenfield.TokenField;

/**
 *
 * @author temp
 */
public class GD2_Bewerber_details {
    
   
   public VerticalLayout mainLayout = new VerticalLayout();
   public HorizontalLayout persDaten = new HorizontalLayout();
   public VerticalLayout persDaten_row1 = new VerticalLayout();
   public VerticalLayout persDaten_row2 = new VerticalLayout();
   public VerticalLayout persDaten_row3 = new VerticalLayout();
   public VerticalLayout berufDaten = new VerticalLayout();
   public HorizontalLayout zust = new HorizontalLayout();
   public VerticalLayout zust_row1 = new VerticalLayout();
   public VerticalLayout zust_row2 = new VerticalLayout();
   public VerticalLayout notizen = new VerticalLayout();
   public VerticalLayout docs = new VerticalLayout();
   public TabSheet tabs = new TabSheet();
   public Window details_window = new Window();
   public Button schliessen = new Button("Schliessen"); 
   public TextField memo = new TextField("Notizen:");
   Button save = new Button("Speichern");
   public TokenField f = new TokenField("Tags");
     public static BeanContainer<String, GD2_BewerberLebenslauf_datasource> lebenslauf_bean = new BeanContainer<>(GD2_BewerberLebenslauf_datasource.class);
    
     
     
     
     
   public Panel panel_docs = new Panel();
   public Panel panel_persDaten = new Panel();
   public Panel panel_berufDaten = new Panel();
   public Panel panel_zust = new Panel();
   public Panel panel_aktionen = new Panel();

   public Panel panel_notizen = new Panel(); 
 public Window getWindow(Object column){
 
     
  schliessen.setStyleName("primary");
  tabs.addTab(panel_persDaten,"Persönliche Daten");
  tabs.addTab(panel_berufDaten,"Berufliche Daten");
  tabs.addTab(panel_zust,"Zuständigkeit");
  tabs.addTab(panel_docs,"Dokumente");
  tabs.addTab(panel_aktionen,"Aktionen");
  tabs.addTab(panel_notizen,"Notizen");
 
 
 //memo.setWidth(60, Sizeable.Unit.PERCENTAGE);
 //memo.setValue("Bisher telefonisch nicht erreicht, 21.03.2016");
 
 
 Embedded em = new Embedded("", new ThemeResource("images/Sebastian_smallpng.png"));
        
 persDaten_row3.addComponent(em);

em.setSizeFull();

 f.removeAllValidators();
 f.setFilteringMode(FilteringMode.OFF);
 f.addToken("Fleissig");
 f.addToken("Gut");
 f.addToken("schnell");
 f.setSizeUndefined();
 f.setHeightUndefined();
 f.setHeight(15, Sizeable.Unit.PERCENTAGE);
     
 
   memo.setWidthUndefined();
   memo.setWidth(80, Sizeable.Unit.PERCENTAGE);
 mainLayout.addComponent(tabs);
 tabs.setSizeFull();
 //mainLayout.addComponent(new Label("<hr />",Label.CONTENT_XHTML));
 mainLayout.addComponent(schliessen);
 //mainLayout.addComponent(new Label("<hr />",Label.CONTENT_XHTML));
 //mainLayout.addComponent(memo);
 //mainLayout.addComponent(save);
 //mainLayout.addComponent(comment_table);
 //mainLayout.addComponent(f);
 mainLayout.setMargin(true);
 mainLayout.setSpacing(true);
 mainLayout.setSizeFull();
 mainLayout.setExpandRatio(schliessen, 0.1f);
mainLayout.setExpandRatio(tabs, 0.9f);
 
 mainLayout.setComponentAlignment(schliessen, Alignment.MIDDLE_LEFT);
 
 
 
 
  schliessen.addClickListener(new Button.ClickListener(){
    
    public void buttonClick(Button.ClickEvent event){
    
        details_window.close();
    }

      
    });
  Label vorName = new Label("Sebastian");
  Label nachName = new Label("Manemann");
  Label adresse = new Label("Höhenstraße 36a");
  Label PLZ = new Label("51491");
  Label ort = new Label("Overath");
  Label Geburtsdatum = new Label("20.01.1985");
  Label nationalitaet  = new Label("Deutsch");
  Label sprachen = new Label("Deutsch, Englisch");
  Label fschein = new Label("Klasse B");

  
  
  vorName.setCaption("Vorname:");
  nachName.setCaption("Nachname:");
  adresse.setCaption("Adresse:");
  PLZ.setCaption("PLZ:");
  ort.setCaption("Ort:");
  Geburtsdatum.setCaption("Geburtsdatum:");
  nationalitaet.setCaption("Nationalität:");
  sprachen.setCaption("Sprachen:");
  fschein.setCaption("Führerschein:");
 
  
  persDaten_row1.addComponent(vorName);
  persDaten_row1.addComponent(nachName);
  persDaten_row1.addComponent(adresse);
  persDaten_row1.addComponent(PLZ);
  persDaten_row1.addComponent(ort);
  persDaten_row2.addComponent(Geburtsdatum);
  persDaten_row2.addComponent(nationalitaet);
  persDaten_row2.addComponent(sprachen);
  persDaten_row2.addComponent(fschein);
  
  
  persDaten.addComponent(persDaten_row1);
  persDaten.addComponent(persDaten_row2);
  persDaten.addComponent(persDaten_row3);
  
  
  panel_persDaten.setContent(persDaten);
  panel_persDaten.setSizeFull();
 
  
  persDaten_row1.setMargin(true);
  persDaten_row1.setSpacing(true);
  persDaten_row2.setMargin(true);
  persDaten_row2.setSpacing(true);
  
  
     final Grid lebenslauf_table = new Grid();
    lebenslauf_table.setContainerDataSource(lebenslauf_bean);
   lebenslauf_bean.setBeanIdProperty("vonJahr");
   lebenslauf_table.setColumnOrder("vonJahr","bisJahr","taetigkeit");
   lebenslauf_bean.addBean(new GD2_BewerberLebenslauf_datasource("2006", "2014", "Koordinator Transcoding - WDR mediagroup GmbH","","",false,""));
   lebenslauf_bean.addBean(new GD2_BewerberLebenslauf_datasource("2015", "2016", " Produktmanager - Netorium GmbH","","",false,""));
    lebenslauf_table.setEditorEnabled(false); 
    lebenslauf_table.setSizeUndefined();
    lebenslauf_table.setHeightUndefined();
   lebenslauf_table.setHeight(30, Sizeable.Unit.PERCENTAGE);
   lebenslauf_table.setWidth(100, Sizeable.Unit.PERCENTAGE);
   
   
  
  
  
  
  
  Label bbz = new Label("Software Entwickler");
  bbz.setCaption("Berufsbezeichnung:");
  Label workedAs = new Label("Systemadministrator, Projektmanager, Produktmanager");
  workedAs.setCaption("Ausgeübte Berufe:");
  Label zusQuali = new Label("Projekt Manager IHK, IBM Sales Professional, IBM Aspera tech. Professional");
  zusQuali.setCaption("Zusatz Qualifikationen:");
  Label sprachenLa = new Label("Deutsch,Englisch");
  sprachenLa.setCaption("Sprachen:");
  lebenslauf_table.setCaption("Lebenslauf:");
  Label lebenslauf = new Label("Lebenslauf:");
  
  berufDaten.addComponent(bbz);
  //berufDaten.addComponent(workedAs);
  //berufDaten.addComponent(lebenslauf);
  berufDaten.addComponent(lebenslauf_table);
  berufDaten.addComponent(zusQuali);
  berufDaten.addComponent(sprachenLa);
  
  berufDaten.setMargin(true);
  berufDaten.setSpacing(true);
  
  panel_berufDaten.setContent(berufDaten);
  panel_berufDaten.setSizeFull();
  
  Label bewIDgen = new Label("2016011000");
bewIDgen.setCaption("Bewerber ID:");
  Label zustStandort = new Label("Leverkusen");
  zustStandort.setCaption("Aktuell zuständige Niederlassung:");
  Label erfasstStandort = new Label("Neuss");
  erfasstStandort.setCaption("Erfasst von Niederlassung:");
  Label erfasstPerson = new Label("hgrimm");
  erfasstPerson.setCaption("Erfasst von Person:");
  Label eingangBW = new Label ("21.03.2016");
  eingangBW.setCaption("Eingang der Bewerbung:");
  Label zztBearb = new Label ("Leverkusen: hgrimm ; Neuss: smanemann");
  zztBearb.setCaption("Zurzeit Bearbeitet von:");
  
  CheckBox isActive = new CheckBox("Ist Aktiv");
  isActive.setValue(true);
  isActive.setEnabled(false);
  CheckBox isArchive = new CheckBox("Ist archiviert");
  isArchive.setValue(false);
  isArchive.setEnabled(false);
  zust_row1.addComponent(bewIDgen);
  zust_row1.addComponent(zustStandort);
  zust_row1.addComponent(erfasstStandort);
  zust_row1.addComponent(erfasstPerson);
  zust_row2.addComponent(eingangBW);
  zust_row2.addComponent(zztBearb);
  
  zust_row2.addComponent(isActive);
  zust_row2.addComponent(isArchive);
  zust_row1.setMargin(true);
  zust_row1.setSpacing(true);
  zust_row2.setMargin(true);
  zust_row2.setSpacing(true);
zust.addComponent(zust_row1);
zust.addComponent(zust_row2);
panel_zust.setContent(zust);
panel_zust.setSizeFull();
  
  
  
Label lebenslauf_bisher = new Label("Bisher gespeicherter Lebenslauf:");
Link lebenslauf_path = new Link("20160321_Lebenslauf_SM_aktuell.pdf", new ExternalResource("www.google.com"));

Label cover_bisher = new Label("Bisher gespeichertes Anschreiben:");
Link cover_path = new Link("anschreiben_SM.pdf", new ExternalResource("www.google.com"));


Label resume_bisher = new Label("Bisher gespeicherte Zeugnisse:");
Link resume_path = new Link("Zeugnisse_vollstaendig.pdf", new ExternalResource("www.google.com"));

Label profbild_bisher = new Label("Bisher gespeichertes Profilbild:");
Link profbild_path = new Link("Sebastian.png", new ExternalResource("www.google.com"));

Label weitere_bisher = new Label("Weitere Anhänge:");
Link weitere_path = new Link("gesammelte_werke.pdf", new ExternalResource("www.google.com"));


docs.addComponent(lebenslauf_bisher);
docs.addComponent(lebenslauf_path);
docs.addComponent(cover_bisher);
docs.addComponent(cover_path);
docs.addComponent(resume_bisher);
docs.addComponent(resume_path);
docs.addComponent(profbild_bisher);
docs.addComponent(profbild_path);
docs.addComponent(weitere_bisher);
docs.addComponent(weitere_path);

docs.setMargin(true);
docs.setSpacing(true);
 
panel_docs.setContent(docs);
panel_docs.setSizeFull();


        details_window.setWidth(70, Sizeable.Unit.PERCENTAGE);
        details_window.setHeight(80, Sizeable.Unit.PERCENTAGE);
        details_window.center();
    
        UI ui = UI.getCurrent();
        details_window.setContent(mainLayout);
        ui.addWindow(details_window);
        
        
   notizen.addComponent(memo);
   notizen.addComponent(save);
   notizen.setMargin(true);
   notizen.setSpacing(true);
   panel_notizen.setContent(notizen);
   panel_notizen.setSizeFull();
   
   
   
   save.addClickListener(new Button.ClickListener(){
   public void buttonClick(Button.ClickEvent event){
       DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
       Date date = new Date();
       System.out.println(dateFormat.format(date));

       
       notizen.addComponent(new Label(dateFormat.format(date)+" - "+VaadinSession.getCurrent().getAttribute("User").toString()+" - "+ memo.getValue()));
       memo.setValue("");
    
    }

      
    });
 
 return details_window;
 }    
    
    
    
}
